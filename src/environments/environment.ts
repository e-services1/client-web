// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'https://localhost:5001',
  media: {
    stay :"/staymedia/",
    adventure:"/adventuremedia/",
    product:"/productmedia/",
    client:"/ClientImages/",
    event :"/event/",
    shop : "/shopMedia/"
  },
  event : {
    getAll :'/events',
  },
  client : {
    outh :{
      login : '/client/outh',
      signup :'/client',
      requestresetpassword :'/client/requestresetpassword/',
      resetpassword :'/client/resetpassword/',
      loginWithFacebook:'/client/signinorsignupWithFacebook/'
    },
    crud :{
      update :'/client/',
      updatePhoto:'/client/picture/'

    },
    stroy :{
      postFollowProduct:'/clientstory/followproduct/',
      unFollowProduct:'/clientstory/unfollowproduct/',
      getFollowProduct:'/clientstory/followproduct/',
      postFollowShop:'/clientstory/followShop/',
      unFollowShop:'/clientstory/unfollowShop/',
      getFollowShop:'/clientstory/followShop/',
      postOrder:'/clientstory/order/',
      getOrders:'/clientstory/orders/',
      getOrderById:'/clientstory/order/',
      deleteOrder:'/clientstory/order/'
    }
  },
  shop :{
    getbyid :'/shop/'
  },
  product: {
    stay:{
      getAll:'/product/stay/',
      getbyId:'/product/stay/'
    },
    adventure :{
      getAll:'/product/adventure/',
      getbyId:'/product/adventure/'
    },
    product:{
      getAll:'/product/product/',
      getbyId:'/product/product/'
    },
    story:{
      search:{
        byCategory:'/productstory/searchbycategory',
        byName:'/productstory/searchbyname'
      }
    }
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
