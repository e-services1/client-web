import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { VisiteurGuard } from './helpers/visiteur.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Role } from './models/role';
import { AuthGuard } from './helpers/auth.guard';


const routes: Routes = [
  {
    path: 'visiteur',
    loadChildren: () => import(`./visiteur/visiteur.module`).then(m => m.VisiteurModule),
    canActivate: [VisiteurGuard]
  },
  {
    path: 'hoster',
    loadChildren: () => import(`./hoster/hoster.module`).then(m => m.HosterModule)
  },
  {
    path: 'forgotpassword/:token',component:ForgotpasswordComponent
  },
  {
    path: 'client',
    loadChildren: () => import(`./client/client.module`).then(m => m.ClientModule),
    canActivate: [AuthGuard],
    data: { roles: [Role.client] }
  },
  { path: '', redirectTo: 'client', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
