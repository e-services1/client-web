import { AppMaterialModule } from './../app-material/app-material.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './login/login.component';
import { HosterComponent } from './hoster.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HosterRoutingModule } from './hoster-routing.module';


@NgModule({
  declarations: [
    HosterComponent,
    LoginComponent],
  imports: [
    CommonModule,
    HosterRoutingModule,
    NgbModule,
    AppMaterialModule
  ]
})
export class HosterModule { }
