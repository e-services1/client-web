import { LoginComponent } from './login/login.component';
import { HosterComponent } from './hoster.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '', component: HosterComponent, children: [
      {
        path: 'outh', component: LoginComponent
      },
      {
        path: '', redirectTo: 'outh', pathMatch: 'full'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HosterRoutingModule { }
