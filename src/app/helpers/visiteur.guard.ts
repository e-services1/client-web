import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/client/authentication.service';
import { Role } from '../models/role';

@Injectable({
  providedIn: 'root'
})
export class VisiteurGuard implements CanActivate {
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
) { }

  canActivate(next: ActivatedRouteSnapshot,state: RouterStateSnapshot){
    const currentUser = this.authenticationService.currentUserValue;
    if (currentUser) {
        // check if route is restricted by role
        if (currentUser.role==Role.client) {
            // role not authorised so redirect to home page
            this.router.navigate(['client']);
            return false;
        }
    }
    // not logged in so redirect to login page with the return url
    return true;
    }
  
}
