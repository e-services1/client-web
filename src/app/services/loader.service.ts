import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {
  private loaderSubject: BehaviorSubject<boolean>;
  public currentLoader: Observable<boolean>;
  constructor() { 
    this.loaderSubject = new BehaviorSubject<boolean>(false);
    this.currentLoader = this.loaderSubject.asObservable();
  }

  show(){
    this.loaderSubject.next(true)
  }

  hide(){
    this.loaderSubject.next(false);
  }
}
