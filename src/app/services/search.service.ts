import { Search } from './../models/search';
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchService {

  private searchSubject: BehaviorSubject<Search>;
  public currentSearch: Observable<Search>;
  
  constructor() { 
    this.searchSubject = new BehaviorSubject<Search>(null);
    this.currentSearch = this.searchSubject.asObservable();
  }

  setSearch(search : Search){
    this.searchSubject.next(search)
  }
}
