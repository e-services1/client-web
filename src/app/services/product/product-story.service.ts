import { Category } from './../../entity/category';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Product } from 'src/app/entity/product';
@Injectable({
  providedIn: 'root'
})
export class ProductStoryService {

  private localhost = environment.apiUrl;

  constructor(
    private http: HttpClient
  ) { }

  searchByCategory(from: any, size: any, name: string, category: string) {
    const formData = new FormData();
    formData.append('name', name);
    formData.append('category', category);
    formData.append('from', from);
    formData.append('size', size);
    return this.http.post<any>(this.localhost + environment.product.story.search.byCategory, formData)
      .pipe(map(rsp => {
        return rsp;
      }));
  }

  searchByName(name: string) {
    const formData = new FormData();
    formData.append('name', name);
    formData.append('from', "0");
    formData.append('size', "12");
    return this.http.post<any>(this.localhost + environment.product.story.search.byName, formData)
      .pipe(map(rsp => {
        return rsp;
      }));
  }

  getAllByCategory(from: any, size: any, category: Category) {

    if (category == Category.stay) {
      return this.http.get<any>(this.localhost + environment.product.stay.getAll, {
        params: { 'from': from, 'size': size }
      })
        .pipe(map(rsp => {
          return rsp;
        }));
    }

    if (category == Category.product) {
      return this.http.get<any>(this.localhost + environment.product.product.getAll, {
        params: { 'from': from, 'size': size }
      })
        .pipe(map(rsp => {
          return rsp;
        }));
    }

    if (category == Category.adventure) {
      return this.http.get<any>(this.localhost + environment.product.adventure.getAll, {
        params: { 'from': from, 'size': size }
      })
        .pipe(map(rsp => {
          return rsp;
        }));
    }
  }

  getArticleById(id: string, cat: Category) {
    
    if ((cat == Category.stay)||(Category[Category[cat]]==Category[Category.stay])) {
      return this.http.get<any>(this.localhost + environment.product.stay.getbyId+id)
        .pipe(map(rsp => {
          return rsp;
        }));
    }

    if ((cat == Category.adventure)||(Category[Category[cat]]==Category[Category.adventure])) {
      return this.http.get<any>(this.localhost + environment.product.adventure.getbyId+id)
        .pipe(map(rsp => {
          return rsp;
        }));
    }

    if ((cat == Category.product)||(Category[Category[cat]]==Category[Category.product])) {
      return this.http.get<any>(this.localhost + environment.product.product.getbyId+id)
        .pipe(map(rsp => {
          return rsp;
        }));
    }
  }

  getShopById(id:string) {
      return this.http.get<any>(this.localhost + environment.shop.getbyid+id)
        .pipe(map(rsp => {
          return rsp.payload;
        }));
    
  }
}
