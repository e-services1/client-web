import { Event } from './../entity/event';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class EventsService {

  private localhost =  environment.apiUrl ;

  constructor(
    private http : HttpClient
  ) {}

  getEvent(from : any , size :any) {
    const formData = new FormData();
    formData.append('from', from);
    formData.append('size', size);
    return this.http.post<Array<Event>>(this.localhost+environment.event.getAll,formData)
    .pipe(map(rsp => {
      return rsp;
    }));
  }
}
