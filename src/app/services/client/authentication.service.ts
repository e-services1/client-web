import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { OuthResponse } from 'src/app/models/OuthResponse';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { User } from 'src/app/entity/user';
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private localhost = environment.apiUrl;

  private currentUserSubject: BehaviorSubject<OuthResponse>;
  public currentUser: Observable<OuthResponse>;

  constructor(private http: HttpClient, private router: Router) {
    this.currentUserSubject = new BehaviorSubject<OuthResponse>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): OuthResponse {
    return this.currentUserSubject.value;
  }

  login(email: string, password: string) {
    const formData = new FormData();
    formData.append('email', email);
    formData.append('password', password);
    return this.http.post<any>(this.localhost + environment.client.outh.login, formData)
      .pipe(map(OuthResponse => {
        // login successful if there's a jwt token in the response
        if (OuthResponse && OuthResponse.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(OuthResponse));
          this.currentUserSubject.next(OuthResponse);
        }

        return OuthResponse;
      }));
  }

  signup(user: User, password: string) {
    this.logout()
    const formData = new FormData();
    formData.append('email', user.email);
    formData.append('password', password);
    formData.append('firstname', user.firstName);
    formData.append('lastname', user.lastName);
    formData.append('phone', user.phone);
    formData.append('urlPhoto', user.urlPhoto);
    return this.http.post<any>(this.localhost + environment.client.outh.signup, formData)
      .pipe(map(OuthResponse => {
        // login successful if there's a jwt token in the response
        return OuthResponse;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    this.router.navigate(['visiteur'])
    localStorage.removeItem('currentUser');
    localStorage.removeItem('followedProduct');
    this.currentUserSubject.next(null);
  }


  update(user : User) {
    const formData = new FormData();
    formData.append('firstname', user.firstName);
    formData.append('lastname', user.lastName);
    formData.append('email', user.email);
    formData.append('phone', user.phone);
    formData.append('urlphoto', user.urlPhoto);
    return this.http.put<any>(this.localhost + environment.client.crud.update+user._Id, formData)
      .pipe(map(OuthResponse => {
        if (OuthResponse && OuthResponse.payload) {
          let user =this.currentUserValue;
          user.payload=OuthResponse.payload
          localStorage.removeItem('currentUser');
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
        }
        return OuthResponse;
      }));
  }

  updatePhoto(photo : File) {
    const formData = new FormData();
    formData.append('files',photo);
    return this.http.post<any>(this.localhost + environment.client.crud.updatePhoto+this.currentUserValue.payload._Id,formData)
      .pipe(map(rsp => {
        if (rsp && rsp.payload) {
          let user =this.currentUserValue;
          user.payload=rsp.payload
          localStorage.removeItem('currentUser');
          localStorage.setItem('currentUser', JSON.stringify(user));
          this.currentUserSubject.next(user);
        }
        return OuthResponse;
      }));
  }


   RequestForUpdatePassword(email :string) {
    const formData = new FormData();
    formData.append('email',email);
    return this.http.post<any>(this.localhost + environment.client.outh.requestresetpassword,formData)
      .pipe(map(rsp => {
        if(rsp.status ==200){
          return true ;
        }else{
          return false ;
        }
      }));
  }

  ResetPassword(email :string ,password :string , token :string) {
    const formData = new FormData();
    formData.append('email',email);
    formData.append('password',password);
    return this.http.post<any>(this.localhost + environment.client.outh.resetpassword,formData,{
      headers:{Authorization: `Bearer ${token}`}
    })
      .pipe(map(rsp => {
        if(rsp.status ==200){
          return true ;
        }else{
          return false ;
        }
      }));
  }

  loginWithFacebook(id: string, token: string) {
    const formData = new FormData();
    formData.append('token', token);
    return this.http.post<any>(this.localhost + environment.client.outh.loginWithFacebook+id,formData,{
      headers:{Authorization: `Bearer ${token}`}
    })      .pipe(map(OuthResponse => {
        // login successful if there's a jwt token in the response
        if (OuthResponse && OuthResponse.token) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          localStorage.setItem('currentUser', JSON.stringify(OuthResponse));
          this.currentUserSubject.next(OuthResponse);
        }
        return OuthResponse;
      }));
  }



}
