import { Order } from './../../entity/order';
import { map } from 'rxjs/operators';
import { Category } from './../../entity/category';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { BehaviorSubject, Observable } from 'rxjs';
import { followedProduct } from 'src/app/entity/followedProduct';

@Injectable({
  providedIn: 'root'
})
export class ClientStoryService {
   keys = Object.keys(Category).filter(k => typeof Category[k as any] === "number"); 
  private localhost = environment.apiUrl;
  private followedSubject: BehaviorSubject<followedProduct>;
  public currentFollowed: Observable<followedProduct>;
  constructor(
    private http: HttpClient,
    private outhService: AuthenticationService
  ) {
    this.followedSubject = new BehaviorSubject<followedProduct>(JSON.parse(localStorage.getItem('followedProduct')));
    this.currentFollowed = this.followedSubject.asObservable();
    this.getFollowProduct()
  }

  getcurrentFollowedProduct() {
    return this.followedSubject.value
  }

  postFollowProduct(idProduct: string, category: Category) {
    let cat: any = category
    const formData = new FormData();
    formData.append('c', cat);
    formData.append('idproduct', idProduct);
    return this.http.post<any>(this.localhost + environment.client.stroy.postFollowProduct + this.outhService.currentUserValue.payload._Id, formData)
      .pipe(map(rsp => {
        if (rsp.status == 200) {
          localStorage.removeItem('followedProduct')
          localStorage.setItem('followedProduct', JSON.stringify(rsp.payload));
          this.followedSubject.next(rsp.payload);
        }
        return rsp;
      }));
  }

  getFollowProduct() {
    this.http.get<any>(this.localhost + environment.client.stroy.getFollowProduct + this.outhService.currentUserValue.payload._Id)
      .subscribe(rsp => {
        if (rsp.status == 200) {
          localStorage.removeItem('followedProduct')
          localStorage.setItem('followedProduct', JSON.stringify(rsp.payload));
          this.followedSubject.next(rsp.payload);
        }
      }, err => {
        localStorage.removeItem('followedProduct')
        this.followedSubject.next(new followedProduct);
        console.error(err)
      })
  }

  deleteFollowProduct(idProduct: string, category: Category) {
    let cat: any = category
    const formData = new FormData();
    formData.append('c', cat);
    formData.append('idproduct', idProduct);
    return this.http.post<any>(this.localhost + environment.client.stroy.unFollowProduct + this.outhService.currentUserValue.payload._Id, formData)
      .pipe(map(rsp => {
        if (rsp.status == 200) {
          localStorage.removeItem('followedProduct')
          localStorage.setItem('followedProduct', JSON.stringify(rsp.payload));
          this.followedSubject.next(rsp.payload);
        }
      }));
  }

  getFollowShop() {
    return this.http.get<any>(this.localhost + environment.client.stroy.getFollowShop + this.outhService.currentUserValue.payload._Id)
      .pipe(map(rsp => {
        if (rsp.status == 200) {
          return rsp.payload
        } else {
          return null;
        }
      }));
  }

  postFollowShop(idShop: string) {
    const formData = new FormData();
    formData.append('idshop', idShop);
    return this.http.post<any>(this.localhost + environment.client.stroy.postFollowShop + this.outhService.currentUserValue.payload._Id, formData)
      .pipe(map(rsp => {
        if (rsp.status == 200) {
          return rsp.payload
        } else {
          return null;
        }
      }));
  }

  deleteFollowShop(idShop: string) {
    const formData = new FormData();
    formData.append('idshop', idShop);
    return this.http.post<any>(this.localhost + environment.client.stroy.unFollowShop + this.outhService.currentUserValue.payload._Id, formData)
      .pipe(map(rsp => {
        if (rsp.status == 200) {
          return rsp.payload
        } else {
          return null;
        }
      }));
  }

  //order
  postOrder(order: Order) {
    //console.warn(Category[Category[order.category]])
    const formData = new FormData();
    formData.append('idProduct', order.idProduct);
    formData.append('category',Category[Category[order.category]]);
    formData.append('timestamp', order.timestamp);
    formData.append('verifyed', order.verifyed.valueOf().toString());
    return this.http.post<any>(this.localhost + environment.client.stroy.postOrder + this.outhService.currentUserValue.payload._Id, formData)
      .pipe(map(rsp => {
        if (rsp.status == 200) {
          return true
        }
        return false;
      }));
  }
  getOrderById(id: string) {
    return this.http.get<any>(this.localhost + environment.client.stroy.getOrderById + this.outhService.currentUserValue.payload._Id
      , { params: { idorder: id } })
      .pipe(map(rsp => {
        if (rsp.status == 200) {
          return rsp.payload
        }
        return null;
      }));
  }
  getOrders() {
    return this.http.get<any>(this.localhost + environment.client.stroy.getOrders + this.outhService.currentUserValue.payload._Id)
      .pipe(map(rsp => {
        if (rsp.status == 200) {
          return rsp.payload
        }
        return null;
      }));
  }
  deleteOrder(id: string) {
    return this.http.delete<any>(this.localhost + environment.client.stroy.getOrderById + this.outhService.currentUserValue.payload._Id
      , { params: { idorder: id } })
      .pipe(map(rsp => {
        if (rsp.status == 200) {
          return rsp.payload
        }
        return null;
      }));

  }
}
