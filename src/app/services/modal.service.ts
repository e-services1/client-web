import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  private loginSubject: BehaviorSubject<boolean>;
  public currentLogin: Observable<boolean>;
  constructor() { 
    this.loginSubject = new BehaviorSubject<boolean>(false);
    this.currentLogin = this.loginSubject.asObservable();
  }

  showLogin(){
    this.loginSubject.next(true)
  }

  hideLogin(){
    this.loginSubject.next(false);
  }
}
