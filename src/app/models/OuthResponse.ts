import { Role } from "./role";
import { User } from '../entity/user';
export class OuthResponse {
    role: Role;
    token: string;
    payload: User;
    status : any;
}