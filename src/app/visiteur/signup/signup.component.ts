import { ModalService } from './../../services/modal.service';
import { Router } from '@angular/router';
import { User } from 'src/app/entity/user';
import { AuthenticationService } from './../../services/client/authentication.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  signupform: FormGroup;
  showLoader :boolean = false ;
  isSubmited : boolean =false  ;
  isCreatedError : boolean ;
  msgError :string ="account already exist" ;
  constructor(
    private formBuilder: FormBuilder,
    private route :Router,
    private authenticationService :AuthenticationService,
    private modal :ModalService
  ) { }

  ngOnInit() {
    this.signupform = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(3)]),
      confirmpassword: new FormControl('', [Validators.required, Validators.minLength(3)]),
      firstname: new FormControl('', [Validators.required, Validators.minLength(3)]),
      lastname: new FormControl('', [Validators.required, Validators.minLength(3)]),
    },
      {
        validator: this.MustMatch('password', 'confirmpassword')
      });
  }

   onSubmit() {
    this.isSubmited=true ;
    this.showLoader=true ;
    this.isSubmited=true;
    this.showLoader=true;
    let user : User = new User() ;
    user.email = this.signupform.value.email;
    user.firstName =this.signupform.value.firstname;
    user.lastName =this.signupform.value.lastname ;
    this.authenticationService.signup(user,this.signupform.value.password)
      .pipe(first())
      .subscribe(
        data => {
          if(data.status==200){
           alert("account created");
          }
        },
        error => {
          console.error(error);
          this.isCreatedError=true ;
          this.showLoader=false;
          this.isSubmited=false
          //this.showLoader=false;
        },
        ()=>{
          this.authenticationService.login(this.signupform.value.email,this.signupform.value.password)
          .pipe(first())
          .subscribe( data =>{
            this.showLoader=false ;
            if(data.status==200){
              this.modal.hideLogin()
              this.route.navigate(['/client']);
            }
          },err =>{
            console.error(err);
            this.isCreatedError=true ;
            this.showLoader=false;
            this.isSubmited=false ;
          });
        }
        );
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }
      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }

}
