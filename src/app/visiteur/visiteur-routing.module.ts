import { SearchResponseComponent } from './search-response/search-response.component';
import { SearchRequestComponent } from './search-request/search-request.component';
import { VisiteurComponent } from './visiteur.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '', component: VisiteurComponent, children: [
      {
        path: 'req', component: SearchRequestComponent
      },
      {
        path: 'rep/:categorie/:subject', component: SearchResponseComponent
      },
      {
        path: '', redirectTo: 'req', pathMatch: 'full'
      },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VisiteurRoutingModule { }
