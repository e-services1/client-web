import { ModalService } from './../../services/modal.service';
import { Router } from '@angular/router';
import { OuthResponse } from 'src/app/models/OuthResponse';
import { AuthenticationService } from './../../services/client/authentication.service';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { LoginComponent } from '../login/login.component';
import { SignupComponent } from '../signup/signup.component';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Output() Navigate = new EventEmitter();
  dialogState = 0;

  navigateTo(element: string) {
    this.Navigate.emit(element)
  }

  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router :Router,
    private currentUser: AuthenticationService,
    private modalService : ModalService
  ) { }

  Login(): void {
    if (this.dialogState == 0) {

      const dialogRef = this.dialog.open(LoginComponent, {
      });

      dialogRef.afterOpen().subscribe(rslt => {
        this.dialogState = 1;
        this.modalService.showLogin();
      })

      dialogRef.afterClosed().subscribe(result => {
        this.dialogState = 0;
        this.modalService.hideLogin();
      });
    }
  }

  Signup(): void {
    if (this.dialogState == 0) {

      const dialogRef = this.dialog.open(SignupComponent, {
      });

      dialogRef.afterOpen().subscribe(rslt => {
        this.dialogState = 1;
      })

      dialogRef.afterClosed().subscribe(result => {
        this.dialogState = 0;
      });
    }
  }

  openHelp() {
    this.snackBar.openFromComponent(HelpSnackBar, {
      duration: 5000,
      panelClass: ['Helpsnackbar']
    });
  }

  ngOnInit() {
    /*this.currentUser.currentUser.subscribe((data :OuthResponse) => {
      if (data!=null){
        if(data.token!=null){
          this.dialog.closeAll();
        }
      }
    });*/
    this.modalService.currentLogin.subscribe(data=>{
      if(!data){
        this.dialog.closeAll();
      }
    })
  }

  gotoHoster(){
    this.router.navigate(['hoster']);
  }

}

@Component({
  selector: 'Help',
  templateUrl: 'snackBar.html',
})
export class HelpSnackBar {

  
}