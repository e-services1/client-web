import { VisiteurComponent } from './visiteur.component';
import { SearchRequestComponent, TermsComponent } from './search-request/search-request.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VisiteurRoutingModule } from './visiteur-routing.module';
import { SearchResponseComponent, searchResponsesnackbar } from './search-response/search-response.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NavbarComponent, HelpSnackBar } from './navbar/navbar.component'
import { SharedModule } from '../shared/shared.module';
import { AppMaterialModule } from '../app-material/app-material.module';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    VisiteurComponent,
    SearchRequestComponent,
    SearchResponseComponent,
    NavbarComponent,
    LoginComponent,
    SignupComponent,
    HelpSnackBar,
    TermsComponent,
    searchResponsesnackbar
  ],
  imports: [
    CommonModule,
    VisiteurRoutingModule,
    FlexLayoutModule,
    FontAwesomeModule,
    SharedModule,
    AppMaterialModule,
    ReactiveFormsModule,
    FormsModule
  ],
  entryComponents: [
    LoginComponent,
    SignupComponent,
    HelpSnackBar,
    TermsComponent,
    searchResponsesnackbar
    ]
})
export class VisiteurModule { }
