import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material';
import { Category } from 'src/app/entity/category';

@Component({
  selector: 'app-search-request',
  templateUrl: './search-request.component.html',
  styleUrls: ['./search-request.component.css'],
})
export class SearchRequestComponent implements OnInit {

  category = Object.keys(Category).filter(k => typeof Category[k as any] === "number");
  isSubmited : boolean = false ;
  searchFrom : FormGroup ;
  
  constructor(
    private route: Router,
    private fb : FormBuilder ,
    private _bottomSheet: MatBottomSheet) { }

  ngOnInit() {
    this.searchFrom = this.fb.group({
      productName: ['', [Validators.required]],
      productCategory: ['', [Validators.required]]
    })
  }

  onChangeCategory(event) {
    this.searchFrom.controls['productCategory'].setValue(event.target.value, {
      onlySelf: true
    })
  }


  onSubmit() {
    this.isSubmited = true;
    if(this.searchFrom.valid){
     this.route.navigate(['visiteur/rep/'+
     this.searchFrom.controls['productCategory'].value
     +'/'+this.searchFrom.controls['productName'].value+''])
  }
  }

  openButtomSheet() {
    this._bottomSheet.open(TermsComponent);
  }
}
@Component({
  selector: 'terms',
  templateUrl: 'terms.html',
})
export class TermsComponent {

  constructor(private _bottomSheetRef: MatBottomSheetRef<TermsComponent>) { }

  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }
}