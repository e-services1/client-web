import { LoaderService } from './../../services/loader.service';
import { MatSnackBar } from '@angular/material';
import { Category } from './../../entity/category';
import { Unit } from './../../entity/unit';
import { Product } from './../../entity/product';
import { ProductStoryService } from './../../services/product/product-story.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-search-response',
  templateUrl: './search-response.component.html',
  styleUrls: ['./search-response.component.css']
})
export class SearchResponseComponent implements OnInit {
  //images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);
  subject: any;
  categorie: any;
  msg: any;
  url: string = "";
  list: Array<Product>;
  constructor(
    private activateRouter: ActivatedRoute,
    private productService: ProductStoryService,
    private snackBar: MatSnackBar,
    private loader : LoaderService
  ) { }

  ngOnInit() {
    this.categorie = this.activateRouter.snapshot.params.categorie;
    this.subject = this.activateRouter.snapshot.params.subject;
    this.msg = "place to " + this.categorie
    switch (this.categorie) {
      case "adventure": {
        this.url = environment.apiUrl + environment.media.adventure
        break;
      }
      case "product": {
        this.url = environment.apiUrl + environment.media.product
        break;
      } case "stay": {
        this.url = environment.apiUrl + environment.media.stay;
        break;
      }
    }
    this.searchByName();
  }

  searchByName() {
    this.loader.show()
    this.list = new Array();
    this.productService.searchByCategory(0,20,this.subject, this.categorie).subscribe(
      data => {
        let product: Product
        data.payload.forEach(element => {
          product = new Product();
          product = element
          this.list.push(product)
        });
      },
      err => {
        console.error(err);
      },
      () => {
        this.loader.hide()
      }
    );
  }

  alertOuth() {
    this.snackBar.openFromComponent(searchResponsesnackbar, {
      duration: 5 * 1000,
      panelClass: ['Helpsnackbar']
    });
     
  }

}

@Component({
  selector: 'snack-bar-component-example-snack',
  template: '<span class="style">you need to login before</span>',
  styles: [`
    .style {
      color: #484848;
      font-family: myfont;
      font-size: 14px;
      text-align: center;
    }
  `],
})
export class searchResponsesnackbar {}