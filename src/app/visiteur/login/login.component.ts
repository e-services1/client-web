import { ModalService } from './../../services/modal.service';
import { Router } from '@angular/router';
import { AuthenticationService } from './../../services/client/authentication.service';
import { Component, OnInit, NgZone } from '@angular/core';
import { first } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray } from '@angular/forms';
declare var login: Function
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isSubmited: boolean = false;
  isOuthError: boolean;
  showLoader: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private modalService: ModalService,
    private zone: NgZone,
    private router: Router) {
  }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(3)]),
    });
  }

  forgotpassword() {
    this.modalService.hideLogin();
    this.router.navigate(['forgotpassword/request']);
  }

  onSubmit() {
    //  this.submitted = true;

    // stop here if form is invalid
    /* if (this.loginForm.invalid) {
         return;
     }*/

    //  this.loading = true;
    this.isSubmited = true;
    this.showLoader = true;
    this.authenticationService.login(this.loginForm.value.email, this.loginForm.value.password)
      .pipe(first())
      .subscribe(
        data => {
          if (data.status == 200) {
            this.modalService.hideLogin();
            this.router.navigate(['/client']);
            this.isOuthError = false;
            this.showLoader = false;
          }
        },
        error => {
          console.error(error);
          this.isOuthError = true;
          this.showLoader = false;
        });

  }
  loginWithFacebook() {
    var my = this
    this.isSubmited = true;
    this.showLoader = true;
    login().login(function (response) {
      if (response.authResponse) {
        console.log(response.authResponse)
        let token = response.authResponse.accessToken
        let id = response.authResponse.userID
        my.zone.run(()=>{
          my.authenticationService.loginWithFacebook(id, token)
            .pipe(first())
            .subscribe(
              data => {
                if (data.status == 200) {
                  my.modalService.hideLogin();
                  my.router.navigate(['/client']);
                  my.isOuthError = false;
                  my.showLoader = false;
                }
              },
              error => {
                console.error(error);
                my.isOuthError = true;
                my.showLoader = false;
              });
        })
      } else {
        console.log('User cancelled login or did not fully authorize.');
      }
    }, {
      scope: 'email',
      return_scopes: true
    }, {
      scope: 'photos',
      return_scopes: true
    });



  }




}
