import { LoaderService } from './../services/loader.service';
import { Component, OnInit, ViewChild ,ElementRef } from '@angular/core';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar, MatSnackBarConfig } from '@angular/material';
@Component({
  selector: 'app-visiteur',
  templateUrl: './visiteur.component.html',
  styleUrls: ['./visiteur.component.css']
})
export class VisiteurComponent implements OnInit {

  showLoader:boolean;

  message: string = 'Snack Bar opened.';
  actionButtonLabel: string = 'Retry';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 2000;
  horizontalPosition: MatSnackBarHorizontalPosition  = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  @ViewChild("footer", { static: false }) footer: ElementRef;

  navigateTo(element: string) {
    this.footer.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" })
  }
 
  constructor(
    private _snackBar: MatSnackBar,
    private loader : LoaderService 
    ) {}

  openSnackBar() {
    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
    this._snackBar.open(this.message, this.action ? this.actionButtonLabel : undefined, config);
  }


  ngOnInit() {
   this.progressloader()
  }

  progressloader(){
    this.loader.currentLoader.subscribe(data=>{
      if(data){
        this.showLoader=true;
      }else{
        this.showLoader=false;
      }
    })
  }


 
}
