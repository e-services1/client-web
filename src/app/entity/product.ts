import { Unit } from './unit';
import { Category } from './category';
export class Product {
    id:string ;
    idShop:string ;
    idHoster :string ;
    category:Category;
    title:string ;
    description:string;
    cost:any;
    like:any;
    urlPhotos:Array<string> ;
}