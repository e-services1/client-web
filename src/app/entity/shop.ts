export class Shop {
    _IdShop :string 
    _IdHoster :string;
    name :string ;
    description :string ;
    address :string ;
    urlPhoto :string ;
    urlVideo :string ;
    lat :string ;
    lan :string ;
    pStay :Array<string>;
    pAdventure:Array<string>;
    pProduct:Array<string>;
}