export class User {
    _Id: string ;
    firstName: string ;
    lastName: string ;
    email: string;
    urlPhoto: string ;
    phone: string;
}
