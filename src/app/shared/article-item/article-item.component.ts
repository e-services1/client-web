import { Category } from './../../entity/category';
import { environment } from 'src/environments/environment';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-article-item',
  templateUrl: './article-item.component.html',
  styleUrls: ['./article-item.component.css']
})
export class ArticleItemComponent implements OnInit {

  @Input() title: string;
  @Input() description: string;
  @Input() id: any;
  @Input() img: Array<string>;
  @Input() like: any;
  @Input() category: Category;
  @Input() cost: any;

  staticFoler: string;
  stayMedia: string = environment.apiUrl + environment.media.stay;
  productMedia: string = environment.apiUrl + environment.media.product;
  adventureMedia: string = environment.apiUrl + environment.media.adventure;

  @Output() clic = new EventEmitter<boolean>();

  submit() {
    this.clic.emit(this.id);
  }

  constructor() {
  }

  ngOnInit() {
    console.log(this.cost)
    if (parseInt(Category[this.category]) == Category.stay) {
      this.staticFoler = this.stayMedia
    }

    if (parseInt(Category[this.category]) == Category.adventure) {
      this.staticFoler = this.adventureMedia
    }

    if (parseInt(Category[this.category]) == Category.product) {
      this.staticFoler = this.productMedia
    }

  }


}
