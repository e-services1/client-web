import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-timestamp-to-date',
  templateUrl: './timestamp-to-date.component.html',
  styleUrls: ['./timestamp-to-date.component.css']
})
export class TimestampToDateComponent implements OnInit {
  @Input() timestamp: any;
  constructor() { }

  ngOnInit() {
  }

}
