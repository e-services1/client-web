import { LoaderService } from './../../services/loader.service';
import { ClientStoryService } from './../../services/client/client-story.service';
import { Order } from './../../entity/order';
import { Router, ActivatedRoute } from '@angular/router';
import { Category } from './../../entity/category';
import { first } from 'rxjs/operators';
import { Product } from './../../entity/product';
import { ProductStoryService } from './../../services/product/product-story.service';
import { User } from './../../entity/user';
import { AuthenticationService } from './../../services/client/authentication.service';
import { Component, OnInit, Inject, NgZone, ChangeDetectorRef } from '@angular/core';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material/bottom-sheet';
import { MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-order-asking',
  templateUrl: './order-asking.component.html',
  styleUrls: ['./order-asking.component.css']
})
export class OrderAskingComponent implements OnInit {
  userValue: User;
  productValue: Product = new Product();
  isLoading: boolean = true;
  checked = false;
  dataConfirmed$:boolean=false;
  orderButton="Confirm my order"
  constructor(
    private user: AuthenticationService,
    private product: ProductStoryService,
    private story: ClientStoryService,
    private router: Router,
    private loader: LoaderService,
    private cdRef:ChangeDetectorRef,
    private _bottomSheetRef: MatBottomSheetRef<OrderAskingComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any) {
    console.log(data.id, data.category)
    router.events.subscribe((val) => {
      this.closeSheet()
    });
  }

  ngOnInit() {
    this.userValue = this.user.currentUserValue.payload;
    this.getProductDetail();
  }
  getProductDetail() {
    var cat: Category = Category[<string>this.data.category];
    this.product.getArticleById(this.data.id, cat)
      .pipe(first())
      .subscribe(data => {
        this.productValue = data;
        this.isLoading = false;
      }, err => {
        this.isLoading = false;
        console.error(err)
      })
  }

  closeSheet() {
    this._bottomSheetRef.dismiss();
  }

  confirmOrder() {
    this.orderButton="Loading ..."
    this.checked=false
    this.loader.show();
    let order: Order = new Order();
    order.idProduct=this.data.id
    order.category = this.data.category;
    order.timestamp = new Date().getTime();
    order.verifyed = false
    this.story.postOrder(order).pipe(first())
      .subscribe(data => {
        this.loader.hide();
        this.dataConfirmed$=data;
        this.cdRef.detectChanges();
      }, err => {
        this.loader.hide()
        console.error(err)
      })

  }

  gotoOrder(){
    this.router.navigate(['client/orders'])
  }

}
