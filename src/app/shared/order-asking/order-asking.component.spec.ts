import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderAskingComponent } from './order-asking.component';

describe('OrderAskingComponent', () => {
  let component: OrderAskingComponent;
  let fixture: ComponentFixture<OrderAskingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderAskingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderAskingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
