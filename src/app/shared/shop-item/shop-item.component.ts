import { environment } from './../../../environments/environment';
import { Shop } from 'src/app/entity/shop';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-shop-item',
  templateUrl: './shop-item.component.html',
  styleUrls: ['./shop-item.component.css']
})
export class ShopItemComponent implements OnInit {
  
  @Input() shop: Shop;
  @Output() clic = new EventEmitter<string>();
  urlPhoto:string=environment.apiUrl+environment.media.shop

  submit() {
    console.log('from card'+this.shop._IdShop)
    this.clic.emit(this.shop._IdShop);
  }

  constructor() { }

  ngOnInit() {
  }

}
