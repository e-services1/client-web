import { FormsModule } from '@angular/forms';
import { ExploreComponent } from './../explore/explore.component';
import { FooterComponent } from './../footer/footer.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppMaterialModule } from '../app-material/app-material.module';
import { ArticleItemComponent } from './article-item/article-item.component';
import { TimestampToDateComponent } from './timestamp-to-date/timestamp-to-date.component';
import { ShopItemComponent } from './shop-item/shop-item.component';
import { OrderAskingComponent } from './order-asking/order-asking.component';
import { OrderItemComponent } from './order-item/order-item.component';
@NgModule({
  declarations: [
    FooterComponent,
    ExploreComponent,
    ArticleItemComponent,
    TimestampToDateComponent,
    ShopItemComponent,
    OrderAskingComponent,
    OrderItemComponent
  ],
  imports: [
    CommonModule,
    AppMaterialModule,
    FormsModule
  ],
  exports: [
    FooterComponent,
    ExploreComponent,
    ArticleItemComponent,
    TimestampToDateComponent,
    ShopItemComponent,
    OrderAskingComponent,
    OrderItemComponent

  ],
  entryComponents: [
    OrderAskingComponent
  ]
})
export class SharedModule { }
