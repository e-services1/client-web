import { environment } from './../../../environments/environment';
import { Product } from './../../entity/product';
import { Order } from './../../entity/order';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Category } from 'src/app/entity/category';

@Component({
  selector: 'app-order-item',
  templateUrl: './order-item.component.html',
  styleUrls: ['./order-item.component.css']
})
export class OrderItemComponent implements OnInit {

  @Input() order: Order;
  @Output() detail = new EventEmitter<Order>();
  @Output() delete = new EventEmitter<string>();
  @Input() product :Product

  viewDetail() {
    this.detail.emit(this.order);
  }
  Fundelete() {
    this.delete.emit(this.order.id);
  }
  path : string ="";

  constructor() { }

  ngOnInit() {
    
  }

  getPath(){
      if(Category[this.product.category]==Category[Category[Category.product]]){
      this.path=environment.apiUrl+environment.media.product
    }
    if(Category[this.product.category]==Category[Category[Category.adventure]]){
      this.path=environment.apiUrl+environment.media.adventure
    }
    if(Category[this.product.category]==Category[Category[Category.stay]]){
      this.path=environment.apiUrl+environment.media.stay
    }
    return this.path
  }



}
