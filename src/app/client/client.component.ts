import { LoaderService } from './../services/loader.service';
import { AuthenticationService } from './../services/client/authentication.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  showLoader:boolean;
  @ViewChild("clientFooter", { static: false }) footer: ElementRef;
  navigateTo(element: string) {
    this.footer.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" })
  }

  constructor(private loader :LoaderService) { }

  ngOnInit() {
    this.progressloader()
  }

  progressloader(){
    this.loader.currentLoader.subscribe(data=>{
      if(data){
        console.warn('loader is active')
        this.showLoader=true;
      }else{
        this.showLoader=false;
        console.warn('loader is inactive')
      }
    })
  }

}
