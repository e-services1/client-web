import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ClientComponent } from './client.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClientRoutingModule } from './client-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { FollowedProductComponent } from './followed-product/followed-product.component';
import { AppMaterialModule } from '../app-material/app-material.module';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ProfileComponent } from './profile/profile.component';
import { FeedComponent } from './feed/feed.component';
import { MessagesComponent } from './messages/messages.component';
import { EventsComponent } from './events/events.component';
import { FollowedShopComponent } from './followed-shop/followed-shop.component';
import { OrdersComponent } from './orders/orders.component';
import { SearchComponent } from './search/search.component';
import { ProductDetialComponent } from './product-detial/product-detial.component';
import { ShopDetailComponent } from './shop-detail/shop-detail.component';


@NgModule({
  declarations: [
    ClientComponent,
    NavbarComponent,
    FollowedProductComponent,
    ProfileComponent,
    FeedComponent,
    MessagesComponent,
    EventsComponent,
    FollowedShopComponent,
    OrdersComponent,
    SearchComponent,
    ProductDetialComponent,
    ShopDetailComponent
  ],
  imports: [
    CommonModule,
    ClientRoutingModule,
    AppMaterialModule,
    SharedModule,
    NgbModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class ClientModule { }
