import { Category } from './../../entity/category';
import { element } from 'protractor';
import { Router } from '@angular/router';
import { LoaderService } from './../../services/loader.service';
import { ProductStoryService } from './../../services/product/product-story.service';
import { Product } from './../../entity/product';
import { first } from 'rxjs/operators';
import { ClientStoryService } from './../../services/client/client-story.service';
import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/entity/order';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  orders:Array<Order>;
  products:Array<Product>;
  constructor(
    private story : ClientStoryService,
    private productstory :ProductStoryService,
    private loader : LoaderService,
    private router :Router ,
  ) { }

  ngOnInit() {
    window.scroll(0,0)
    this.getAllOrder()
  }

  getAllOrder(){
    this.orders=new Array<Order>();
    this.products = new Array<Product>();
    this.story.getOrders().pipe(first())
    .subscribe(data=>{
      if(data){
        data.forEach(element => {
          this.story.getOrderById(element).pipe(first())
          .subscribe(data=>{
            if(data){
              this.orders.push(data)
            }
          }).add(()=>{
            this.orders.forEach(element => {
              this.productstory.getArticleById(element.idProduct,element.category)
              .pipe(first())
              .subscribe(data=>{
                if(data){
                  this.products.push(data)
                }
              })
            });
          })
        });
      }
    })
  }

  openDetai($evnet){
    let order : Order = $evnet ;
    this.router.navigate(['client/detail/'+order.category+"/"+order.idProduct])
  }
  deleteRequest($evnet){
    this.story.deleteOrder($evnet).pipe(first()).subscribe(data=>{
      if(data){
        window.location.reload();
      }else{
        alert("sorry we can't delete your order")
      }
    })
    
  }

}
