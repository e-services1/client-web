import { ClientStoryService } from './../../services/client/client-story.service';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { Search } from './../../models/search';
import { SearchService } from './../../services/search.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Category } from 'src/app/entity/category';
import { Component, OnInit, ViewChild, ElementRef, HostListener, EventEmitter, Output } from '@angular/core';
import { AuthenticationService } from 'src/app/services/client/authentication.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  @Output() Navigate = new EventEmitter();
  dialogState = 0;

  navigateTo(element: string) {
    this.Navigate.emit(element)
  }

  img : string ;

  public isCollapsed = true;
  selectable = true;
  removable = true;
  category = Object.keys(Category).filter(k => typeof Category[k as any] === "number");
  search: FormGroup;

  constructor(
    private fb: FormBuilder,
    private searchService :SearchService,
    private router :Router,
    private follow :ClientStoryService,
    private outh : AuthenticationService) {
  }

  remove(cat: string): void {
    if (this.category.length > 1) {
      const index = this.category.indexOf(cat);
      if (index >= 0) {
        this.category.splice(index, 1);
      }
    }
  }
  reset(){
    this.category=Object.keys(Category).filter(k => typeof Category[k as any] === "number");
    this.search.reset
  }
  collapse() {
    this.isCollapsed = !this.isCollapsed;
  }

  ngOnInit() {
    this.search = this.fb.group({
      cost: ['', [Validators.required]],
      value :['',[Validators.required]]
    })

    this.outh.currentUser.subscribe(data=>{
      if(data!=null){
        if((data.payload.urlPhoto!="undefined")&&(data.payload.urlPhoto!=null)){
          if(data.payload.urlPhoto.indexOf('http')>=0){
            this.img=data.payload.urlPhoto
          }else{
            this.img=environment.apiUrl+environment.media.client+data.payload.urlPhoto
          }
        }
      }
    })
    //this.follow.getFollowProduct()
  }

  onSubmit() {
    window.scroll(0,0)
    this.collapse()
    if(this.search.valid){
      let search : Search = new Search();
      search.category=this.category
      search.cost=this.search.controls['cost'].value
      search.value=this.search.controls['value'].value
      this.searchService.setSearch(search)
      this.router.navigate(['client/search'])
    }
  }

  logout(){
    this.outh.logout();
  }

}
