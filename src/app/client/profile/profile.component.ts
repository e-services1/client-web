import { environment } from 'src/environments/environment';
import { User } from 'src/app/entity/user';
import { OuthResponse } from 'src/app/models/OuthResponse';
import { AuthenticationService } from 'src/app/services/client/authentication.service';
import { LoaderService } from './../../services/loader.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  isEnabled: boolean = false;
  isFormError: boolean = false;
  updateForm: FormGroup;
  showLoader: boolean = true;
  currentUser: OuthResponse;
  img :string;
  constructor(
    private fb: FormBuilder,
    private loader: LoaderService,
    private outhService: AuthenticationService
  ) { }

  ngOnInit() {
    window.scroll(0,0);
    this.updateForm = this.fb.group({
      firstname: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      lastname: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      phone: ['', Validators.compose([Validators.required, Validators.pattern("[0-9 ]{8}")])],
      email: ['', Validators.required],
    })
    this.disable()
    this.setData()
  }

  onSubmit() {
    this.disable();
    this.currentUser.payload.firstName = this.updateForm.controls['firstname'].value
    this.currentUser.payload.lastName = this.updateForm.controls['lastname'].value
    this.currentUser.payload.phone = this.updateForm.controls['phone'].value
    this.loader.show();
    this.outhService.update(this.currentUser.payload)
      .pipe(first())
      .subscribe(data => {
        console.log(data)
        this.loader.hide()
      }, err => {
        console.log(err)
        this.loader.hide()
      });

  }

  enable() {
    this.isEnabled = true;
    this.updateForm.get('firstname').enable();
    this.updateForm.get('lastname').enable();
    this.updateForm.get('phone').enable();
  }

  disable() {
    this.updateForm.get('firstname').disable();
    this.updateForm.get('lastname').disable();
    this.updateForm.get('phone').disable();
    this.updateForm.get('email').disable();
  }

  setData() {
    this.outhService.currentUser.subscribe(data => {
      if(data!=null){
        if((data.payload.urlPhoto!="undefined")&&(data.payload.urlPhoto!=null)){
          if(data.payload.urlPhoto.indexOf('http')>=0){
            this.img=data.payload.urlPhoto
          }else{
            this.img=environment.apiUrl+environment.media.client+data.payload.urlPhoto
          }
        }
      }
      this.currentUser = data
      this.updateForm.controls['firstname'].setValue(this.currentUser.payload.firstName);
      this.updateForm.controls['lastname'].setValue(this.currentUser.payload.lastName);
      this.updateForm.controls['email'].setValue(this.currentUser.payload.email);
      if (this.currentUser.payload.phone != "undefined") {
        this.updateForm.controls['phone'].setValue(this.currentUser.payload.phone);
      }
    })
    /*this.outhService.currentUser.subscribe(data=>{
      console.log('sub'+data.payload.firstName)
    })*/
  }

  handleFileInput(files: FileList){
   let fileToUpload: File = null;
   fileToUpload = files.item(0);
   this.loader.show()
   this.outhService.updatePhoto(fileToUpload)
      .pipe(first())
      .subscribe(data => {
        console.log(data)
        this.loader.hide()
      }, err => {
        console.log(err)
        this.loader.hide()
      });

  }

}
