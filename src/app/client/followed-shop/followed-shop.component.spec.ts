import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowedShopComponent } from './followed-shop.component';

describe('FollowedShopComponent', () => {
  let component: FollowedShopComponent;
  let fixture: ComponentFixture<FollowedShopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FollowedShopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowedShopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
