import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { ProductStoryService } from 'src/app/services/product/product-story.service';
import { ClientStoryService } from './../../services/client/client-story.service';
import { LoaderService } from 'src/app/services/loader.service';
import { Shop } from 'src/app/entity/shop';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-followed-shop',
  templateUrl: './followed-shop.component.html',
  styleUrls: ['./followed-shop.component.css']
})
export class FollowedShopComponent implements OnInit {
  shop: Array<Shop> =new Array();
  constructor(
    private loader : LoaderService,
    private story : ClientStoryService,
    private prodStory : ProductStoryService,
    private router :Router
  ) { }

  ngOnInit() {this.getFollowedShop()}

  goShopDetail($event){
    this.router.navigate(['client/shop/'+$event])
  }

  getFollowedShop(){
    this.story.getFollowShop()
    .pipe(first())
    .subscribe(data=>{
      if (data){
        data.forEach(element => {
          this.prodStory.getShopById(element)
          .pipe(first())
          .subscribe(data=>{
            this.shop.push(data)
          })
        });
      }
    })
  }

}
