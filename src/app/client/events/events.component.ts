import { environment } from 'src/environments/environment';
import { Event } from './../../entity/event';
import { EventsService } from './../../services/events.service';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  list :Array<Event> = new Array();
  img :string = environment.apiUrl+environment.media.event;
  isLoading:Boolean=true ;

  constructor(private event :EventsService) { }

  ngOnInit() {
    window.scroll(0,0);
    this.event.getEvent(0,12)
    .pipe(first())
    .subscribe(data=>{
      this.isLoading=false;
      this.list=data
    },err=>{
      this.isLoading=false;
      console.log(err)
    })
  }

}
