import { environment } from './../../../environments/environment';
import { ClientStoryService } from './../../services/client/client-story.service';
import { first } from 'rxjs/operators';
import { ProductStoryService } from './../../services/product/product-story.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LoaderService } from 'src/app/services/loader.service';
import { Shop } from 'src/app/entity/shop';
import { Product } from 'src/app/entity/product';
import { Category } from 'src/app/entity/category';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-shop-detail',
  templateUrl: './shop-detail.component.html',
  styleUrls: ['./shop-detail.component.css']
})
export class ShopDetailComponent implements OnInit {
  position = "center"
  idshop: string;
  shop: Shop;
  shopMediaPath: string = environment.apiUrl + environment.media.shop
  staylist: Array<Product> = new Array();
  adventurelist: Array<Product> = new Array();
  productlist: Array<Product> = new Array();
  isFollow: Boolean;

  constructor(
    private routeData: ActivatedRoute,
    private route: Router,
    private productService: ProductStoryService,
    private loader: LoaderService,
    private story: ClientStoryService,
    private security: DomSanitizer

  ) { }
  photoURL($event){
    return this.security.bypassSecurityTrustResourceUrl($event)
  }

  ngOnInit() {
    window.scroll(0, 0)
    this.idshop = this.routeData.snapshot.paramMap.get('id');
    this.getDetail();
    this.followShop();
  }
  getDetail() {
    this.shop = new Shop();
    this.loader.show();
    this.productService.getShopById(this.idshop)
      .pipe(first())
      .subscribe(data => {
        this.shop = data;
        this.loader.hide();
      }, err => {
        this.loader.hide();
        console.error(err);
      })
      .add(() => {
        if(this.shop.pStay!=null){
          this.shop.pStay.forEach(element => {
            this.productService.getArticleById(element, Category.stay)
              .pipe(first())
              .subscribe(data => {
                this.staylist.push(data)
              })
          });
        }

        if(this.shop.pAdventure!=null){
          this.shop.pAdventure.forEach(element => {
            this.productService.getArticleById(element, Category.adventure)
              .pipe(first())
              .subscribe(data => {
                this.adventurelist.push(data)
              })
          });
        }
       
        if(this.shop.pProduct!=null){
          this.shop.pProduct.forEach(element => {
            this.productService.getArticleById(element, Category.product)
              .pipe(first())
              .subscribe(data => {
                this.productlist.push(data)
              })
          });
        }
        
      })
  }

  openDetail($event, cat: Category) {
    window.scroll(0, 0);
    this.route.navigate(['client/detail/' + cat + '/' + $event + ''])
    console.log($event)
  }

  followShop() {
    let array: Array<string>;
    this.story.getFollowShop()
      .pipe(first())
      .subscribe(data => {
        if (data) {
          array = data;
          if (array.indexOf(this.idshop) >= 0) {
            this.isFollow = true;
          } else {
            this.isFollow = false;
          }
        }
      })
  }

  unfollowShop() {
    this.loader.show()
    let array: Array<string>;
    this.story.deleteFollowShop(this.idshop)
      .pipe(first())
      .subscribe(data => {
        if (data) {
          array = data;
          if (array.indexOf(this.idshop) >= 0) {
            this.isFollow = true;
          } else {
            this.isFollow = false;
          }
        }
      }).add(() => {
        this.loader.hide()
      })
  }
  postFollowShop() {
    this.loader.show()
    let array: Array<string>;
    this.story.postFollowShop(this.idshop)
      .pipe(first())
      .subscribe(data => {
        if (data) {
          array = data;
          if (array.indexOf(this.idshop) >= 0) {
            this.isFollow = true;
          } else {
            this.isFollow = false;
          }
        }
      }).add(() => {
        this.loader.hide()
      })
  }

}
