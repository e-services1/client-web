import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowedProductComponent } from './followed-product.component';

describe('FollowedProductComponent', () => {
  let component: FollowedProductComponent;
  let fixture: ComponentFixture<FollowedProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FollowedProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowedProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
