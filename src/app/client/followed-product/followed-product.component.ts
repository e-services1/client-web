import { Router } from '@angular/router';
import { first, last } from 'rxjs/operators';
import { Category } from './../../entity/category';
import { LoaderService } from './../../services/loader.service';
import { ClientStoryService } from './../../services/client/client-story.service';
import { Component, OnInit } from '@angular/core';
import { ProductStoryService } from 'src/app/services/product/product-story.service';
import { Product } from 'src/app/entity/product';

@Component({
  selector: 'app-followed-product',
  templateUrl: './followed-product.component.html',
  styleUrls: ['./followed-product.component.css']
})
export class FollowedProductComponent implements OnInit {

  haveAdventure: boolean;
  haveStay: boolean;
  haveProduct: boolean;

  arrayStay: Array<Product>= new Array();
  arrayAdventure: Array<Product>= new Array();
  arrayProduct: Array<Product>= new Array();

  constructor(
    private currentFollow: ClientStoryService,
    private loader: LoaderService,
    private productservice: ProductStoryService,
    private router :Router
  ) { }

  ngOnInit() {
    window.scroll(0, 0)
    this.getProduct()
  }

  getProduct() {

    let data = this.currentFollow.getcurrentFollowedProduct() ;

      if ((data.stay != null) && (data.stay.length > 0)) {
        this.haveStay = true;
        data.stay.forEach(element => {
          this.productservice.getArticleById(element, Category.stay)
            .pipe(first())
            .subscribe(data => {
              this.arrayStay.push(data)
            }, err => {
              console.error(err)
            })
        })
      } else {
        this.haveStay = false;
      }

      if ((data.adventure != null) && (data.adventure.length > 0)) {
        this.haveAdventure = true;
        data.adventure.forEach(element => {
          this.productservice.getArticleById(element, Category.adventure)
            .pipe(first())
            .subscribe(data => {
              this.arrayAdventure.push(data)
            }, err => {
              console.error(err)
            })
        })
      } else {
        this.haveAdventure = false;
      }

      if((data.product!=null)&&(data.product.length>0)){
        this.haveProduct = true;
        data.product.forEach(element => {
          this.productservice.getArticleById(element, Category.product)
            .pipe(first())
            .subscribe(data => {
              this.arrayProduct.push(data)
            }, err => {
              console.error(err)
            })
        })
      } else {
        this.haveProduct = false;
      }
  
  }

  openDetail($event, cat: Category) {
   this.router.navigate(['client/detail/'+cat+"/"+$event]) 
  }

}
