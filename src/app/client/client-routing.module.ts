import { ProductDetialComponent } from './product-detial/product-detial.component';
import { SearchComponent } from './search/search.component';
import { OrdersComponent } from './orders/orders.component';
import { MessagesComponent } from './messages/messages.component';
import { FollowedShopComponent } from './followed-shop/followed-shop.component';
import { FeedComponent } from './feed/feed.component';
import { EventsComponent } from './events/events.component';
import { ProfileComponent } from './profile/profile.component';
import { FollowedProductComponent } from './followed-product/followed-product.component';
import { ClientComponent } from './client.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShopDetailComponent } from './shop-detail/shop-detail.component';


const routes: Routes = [
  {
    path: '', component: ClientComponent, children: [
      {
        path: 'events', component: EventsComponent
      },
      {
        path: 'feed', component: FeedComponent
      },
      {
        path: 'followedProduct', component: FollowedProductComponent
      },
      {
        path: 'followedShop', component: FollowedShopComponent
      },
      {
        path: 'messages', component: MessagesComponent
      },
      {
        path: 'profil', component: ProfileComponent
      },
      {
        path: 'orders', component: OrdersComponent
      },
      {
        path: 'search', component: SearchComponent
      },
      {
        path: 'detail/:cat/:id', component: ProductDetialComponent
      },
      {
        path: 'shop/:id' ,component :ShopDetailComponent
      },
      {
        path: '', redirectTo: 'feed', pathMatch: 'full'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ClientRoutingModule { }
