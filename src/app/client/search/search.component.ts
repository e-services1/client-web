import { LoaderService } from './../../services/loader.service';
import { Search } from './../../models/search';
import { ProductStoryService } from './../../services/product/product-story.service';
import { Category } from 'src/app/entity/category';
import { Router } from '@angular/router';
import { SearchService } from './../../services/search.service';
import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/entity/product';
import { first, finalize, last } from 'rxjs/operators';
import { promise } from 'protractor';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  value: Search = new Search();
  list: Array<Product> ;
  size :any ;
  haveWork :boolean =true ;
  constructor(
    private router: Router,
    private prodService: ProductStoryService,
    private loader :LoaderService,
    private searchService: SearchService) {

  }
  ngOnInit() {
    this.searchService.currentSearch.subscribe(data => {
      if (data != null) {
        this.value = data;
        console.log(data)
        this.getProduct()
      } else {
        this.router.navigate(['client/feed'])
      }
    }, err => {
      console.error(err);
    })
  }

  pagination($event){
    console.log($event)
    
  }

  countPaginationSize(length){
    let xx : any =parseInt(length)/parseInt('6')
    xx=parseInt(xx).toPrecision()
    xx++
    this.size=xx
  }


   getProduct() {
    this.list = new Array();
    this.value.category.forEach(element => {
      this.loader.show();
      this.prodService.searchByCategory(0, 6, this.value.value, element)
        .pipe(first())
        .subscribe(data => {
          this.loader.hide();
          if (data.payload != null) {
            data.payload.forEach(element => {
              this.list.push(element)   
              this.haveWork=false            
            });
          }
        }, err => {
          this.loader.hide();
          console.error(err)
        })
        .add(()=>{
          this.countPaginationSize(this.list.length)
          if(this.value.cost=='up'){
            this.list.sort(function(a, b) {
              if (a.cost < b.cost)
                return -1;
              if (a.cost > b.cost)
                return 1;
              return 0;
            });
          }else{
            this.list.sort(function(a, b) {
              if (a.cost > b.cost)
                return -1;
              if (a.cost < b.cost)
                return 1;
              return 0;
            });
          }
        })

    });
  }

  openDetail($event,cat : Category) {
    this.router.navigate(['client/detail/'+cat+'/'+$event+''])
  }


}
