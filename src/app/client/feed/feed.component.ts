import { LoaderService } from './../../services/loader.service';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { ProductStoryService } from './../../services/product/product-story.service';
import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/entity/category';
import { Product } from 'src/app/entity/product';
import { ThrowStmt } from '@angular/compiler';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {
  
  listStay : Array<Product>;
  listProduct : Array<Product>;
  listAdventure : Array<Product>;

  productPage:any =10
  currentProductPage:any=1;

  stayPage:any =10
  currentStayPage:any=1;

  adventurePage:any =10
  currentAdventurePage:any=1;

  constructor(
    private prodService :ProductStoryService,
    private loader :LoaderService,
    private route :Router
  ) {}

  ngOnInit() {
    window.scroll(0,0)
    this.getProducts(1);
    this.getStay(1);
    this.getAdventure(1);
  }

  openDetail($event,cat : Category) {
    window.scroll(0,0);
    this.route.navigate(['client/detail/'+cat+'/'+$event+''])
  }

  getProducts(page:any){
    let from = 4*(page-1)
    this.listProduct=new Array();
    this.loader.show();
    this.prodService.getAllByCategory(from,4,Category.product)
    .pipe(first())
    .subscribe(data=>{
      this.loader.hide();
      this.listProduct = data ;
      if(page==this.currentProductPage){
        this.currentProductPage++
        if(this.listProduct.length==4){
          this.productPage=this.productPage+10;
        }
      }
      
    },err=>{
      console.log(err)
      this.loader.hide();
    })
  }

  getStay(page:any){
    let from = 4*(page-1)
    this.loader.show()
    this.listStay = new Array() ;
    this.prodService.getAllByCategory(from,4,Category.stay)
    .pipe(first())
    .subscribe(data=>{
      this.loader.hide();
      this.listStay = data ;
      if(page==this.currentStayPage){
        this.currentStayPage++
        if(this.listStay.length==4){
          this.stayPage=this.stayPage+10;
        }
      }
    },err=>{
      console.log(err)
      this.loader.hide();
    })
  }

  getAdventure(page:any){
    let from = 4*(page-1)
    this.loader.show();
    this.listAdventure= new Array();
    this.prodService.getAllByCategory(from,4,Category.adventure)
    .pipe(first())
    .subscribe(data=>{
      this.loader.hide();
      this.listAdventure = data ;
      if(page==this.currentAdventurePage){
        this.currentAdventurePage++
        if(this.listAdventure.length==4){
          this.adventurePage=this.adventurePage+10;
        }
      }
    },err=>{
      console.log(err)
      this.loader.hide();
    })
  }


  pagination($event){
    this.getProducts($event)
  }


}
