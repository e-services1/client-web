import { OrderAskingComponent } from './../../shared/order-asking/order-asking.component';
import { ClientStoryService } from './../../services/client/client-story.service';
import { Shop } from './../../entity/shop';
import { LoaderService } from './../../services/loader.service';
import { environment } from './../../../environments/environment';
import { Pstay } from './../../entity/pstay';
import { Padventure } from './../../entity/padventure';
import { ProductStoryService } from './../../services/product/product-story.service';
import { ActivatedRoute, Router} from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { Category } from 'src/app/entity/category';
import { Pproduct } from 'src/app/entity/pproduct';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';

declare var login: Function

@Component({
  selector: 'app-product-detial',
  templateUrl: './product-detial.component.html',
  styleUrls: ['./product-detial.component.css']
})
export class ProductDetialComponent implements OnInit {
  id :string ;
  cat :string ;
  isFollow : boolean =false ;
  product :boolean =false ;
  adventure :boolean =false ;
  stay:boolean =false ;
  itemProduct :Pproduct =new Pproduct();
  itemAdventure :Padventure = new Padventure();
  itemStay:Pstay=new Pstay();
  arrayImg :Array<string> =new Array();
  staticFolder=environment.apiUrl;
  idShop :string; 
  shop:Shop = new Shop();
  staticfolder = environment.apiUrl+environment.media.shop
  cost :any ;
  constructor(
    private routeData: ActivatedRoute,
    private route: Router,
    private productSerive : ProductStoryService,
    private loader :LoaderService,
    private clientStory : ClientStoryService,
    private _bottomSheet: MatBottomSheet
  ) { }

  ngOnInit() {
    window.scroll(0,0)
    this.loader.show()
    this.stay=false ;
    this.adventure=false;
    this.product=false;
    this.id=this.routeData.snapshot.paramMap.get('id');
    this.cat=this.routeData.snapshot.paramMap.get('cat');
    if((this.id!=null)&&(this.cat!=null)){
      this.productSerive.getArticleById(this.id,Category[this.cat])
      .pipe(first())
      .subscribe(data=>{
        if (parseInt(Category[this.cat]) == Category.stay) {
          this.stay=true;
          this.itemStay=data;
          this.arrayImg=this.itemStay.urlPhotos
          this.staticFolder=this.staticFolder+environment.media.stay
          this.idShop =this.itemStay.idShop
          this.cost=data.cost
        }
    
        if (parseInt(Category[this.cat]) == Category.adventure) {
          this.adventure=true;
          this.itemAdventure=data;
          this.arrayImg=this.itemAdventure.urlPhotos
          this.staticFolder=this.staticFolder+environment.media.adventure
          this.idShop =this.itemAdventure.idShop
          this.cost=data.cost

        }
    
        if (parseInt(Category[this.cat]) == Category.product) {
          this.product=true;
          this.itemProduct=data;
          this.arrayImg=this.itemProduct.urlPhotos
          this.staticFolder=this.staticFolder+environment.media.product
          this.idShop =this.itemProduct.idShop
          this.cost=data.cost


        }
        console.log(data)
      },err=>{
        this.loader.hide()
      })
      .add(()=>{
        this.productSerive.getShopById(this.idShop).subscribe(data=>{
          this.loader.hide();
          this.shop =data ;

        },err =>{
          this.loader.hide();
        }).add(()=>{
          this.isFollowedProduct()
        })
      })
    }else{
      this.route.navigate(['client/feed']);
    }

  }

  FacebookShare(){
    login().ui({
      display: 'popup',
      method: 'share',
      href: 'https://youtube.com',
    }, function(response){});
  
  }

  shopDetail(){
    this.route.navigate(['client/shop/'+this.idShop])
  }
  
  postFollowProduct($event,cat:Category){
    this.loader.show()
    this.clientStory.postFollowProduct($event,cat)
    .pipe(first())
    .subscribe(data=>{
      this.loader.hide()
      if(data.status==200){
      }else{
        alert("something went rong while trying to follow")
      }
    },err=>{
      this.loader.hide()
      console.error(err)
    })
  }

  isFollowedProduct(){
   this.clientStory.currentFollowed.subscribe(data=>{
     this.isFollow=false ;
      if(this.product){
        if(data.product!=null){
          if(data.product.indexOf(this.itemProduct.id)>=0){
            this.isFollow=true
          }
        }        
      }
      if(this.stay){
        if(data.stay!=null){
          if(data.stay.indexOf(this.itemStay.id)>=0){
            this.isFollow=true
          }
        }        
      }
      if(data.adventure!=null){
        if(this.adventure){
          if(data.adventure.indexOf(this.itemAdventure.id)>=0){
            this.isFollow=true
          }
        }
      }      
   })
  }

  unFollow($event,cat:Category){
    this.loader.show();
    this.clientStory.deleteFollowProduct($event,cat)
    .pipe(first())
    .subscribe(data=>{
      this.loader.hide();
      this.isFollowedProduct()
    },err =>{
      console.error(err);
      this.loader.hide();
    })
  }

  order(){
    this._bottomSheet.open(OrderAskingComponent,{
      data: { id: this.id,category:this.cat }    });
  }
}
