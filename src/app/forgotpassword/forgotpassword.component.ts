import { LoaderService } from './../services/loader.service';
import { AuthenticationService } from 'src/app/services/client/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  formRequest : FormGroup;
  formReset : FormGroup;
  showSendResetReset :boolean ;
  isRequest: boolean =true ;
  token :string ;

  constructor(
    private fb : FormBuilder,
    private routeData : ActivatedRoute,
    private resetService :  AuthenticationService,
    private router :Router,
    private loader : LoaderService
  ) { }

  ngOnInit() {
    this.formRequest=this.fb.group({
      email :['',Validators.compose([Validators.required, Validators.email])]
    })
    this.formReset=this.fb.group({
      email :['',Validators.compose([Validators.required, Validators.email])],
      password :['',Validators.compose([Validators.required, Validators.minLength(3)])],
      confirmPassword :['',Validators.compose([Validators.required, Validators.minLength(3)])]
    },
    {
      validator: this.MustMatch('password', 'confirmPassword')
    })

    this.token =this.routeData.snapshot.paramMap.get('token');
    
    if(this.token=="request"){
      this.isRequest=true
    }else{
      this.isRequest =false ;
    }
  }

  onRequestSubmit(){
    this.loader.show();
    let email :string = this.formRequest.controls['email'].value ;
    this.resetService.RequestForUpdatePassword(email)
    .pipe(first())
    .subscribe(data=>{
      this.showSendResetReset=data
      this.loader.hide()
    },err=>{
      this.loader.hide()
      console.error(err)
    })
  }

  onResetSubmit(){
    this.loader.show();
    let email :string = this.formReset.controls['email'].value ;
    let password :string = this.formReset.controls['password'].value ;
    this.resetService.ResetPassword(email,password,this.token)
    .pipe(first())
    .subscribe(data=>{
      this.loader.hide()
      console.log(data)
    },err=>{
      console.error(err);
      this.loader.hide();
    })
    .add(()=>{
      this.router.navigate(['visiteur'])
    })
  }

  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }
      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }


}
