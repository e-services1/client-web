(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["hoster-hoster-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/hoster/hoster.component.html":
/*!************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/hoster/hoster.component.html ***!
  \************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-login></app-login>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/hoster/login/login.component.html":
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/hoster/login/login.component.html ***!
  \*****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"row w-100\" style=\"height:100vh;\">\n    <div class=\"col-md-4 col-lg-6 col-xl-6 d-none d-md-block\" style=\"padding: 0px;\">\n        <ngb-carousel>\n            <ng-template ngbSlide>\n                <div class=\"picsum-img-wrapper\">\n                    <div class=\"div1\" alt=\"Random first slide\"></div>\n                </div>\n                <div class=\"carousel-caption\">\n                    <h3>First slide label</h3>\n                    <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>\n                </div>\n            </ng-template>\n            <ng-template ngbSlide>\n                <div class=\"picsum-img-wrapper\">\n                    <div class=\"div2\" alt=\"Random first slide\"></div>\n                </div>\n                <div class=\"carousel-caption\">\n                    <h3>Second slide label</h3>\n                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>\n                </div>\n            </ng-template>\n            <ng-template ngbSlide>\n                <div class=\"picsum-img-wrapper\">\n                    <div class=\"div3\" alt=\"Random first slide\"></div>\n                </div>\n                <div class=\"carousel-caption\">\n                    <h3>Third slide label</h3>\n                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>\n                </div>\n            </ng-template>\n            <ng-template ngbSlide>\n                <div class=\"picsum-img-wrapper\">\n                    <div class=\"div4\" alt=\"Random first slide\"></div>\n                </div>\n                <div class=\"carousel-caption\">\n                    <h3>Third slide label</h3>\n                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>\n                </div>\n            </ng-template>\n            <ng-template ngbSlide>\n                <div class=\"picsum-img-wrapper\">\n                    <div class=\"div5\" alt=\"Random first slide\"></div>\n                </div>\n                <div class=\"carousel-caption\">\n                    <h3>Third slide label</h3>\n                    <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>\n                </div>\n            </ng-template>\n        </ngb-carousel>\n    </div>\n    <div class=\"col-12 col-sm-12 col-md-8 col-lg-6 col-xl-6 d-flex justify-content-center align-items-center\">\n        <div class=\"w-75\">\n            <div class=\"d-flex justify-content-center\">\n                <img src=\"../../../assets/logo/Khomsa logo-02.png\">\n            </div>\n            <ul ngbNav #nav=\"ngbNav\" class=\"nav-tabs d-flex justify-content-center\">\n                <li [ngbNavItem]=\"1\">\n                    <a ngbNavLink>Register</a>\n                    <ng-template ngbNavContent>\n                        <form>\n                            <div class=\"form-group\">\n                                <label for=\"exampleInputEmail1\">Email address</label>\n                                <input type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\"\n                                    aria-describedby=\"emailHelp\">\n                                <small id=\"emailHelp\" class=\"form-text text-muted\">We'll never share your email with\n                                    anyone\n                                    else.</small>\n                            </div>\n                            <div class=\"form-group\">\n                                <label for=\"exampleInputPassword1\">Password</label>\n                                <input type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\">\n                            </div>\n                            <div class=\"form-group\">\n                                <label for=\"exampleInputPassword1\">Phone</label>\n                                <input type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\">\n                            </div>\n                            <div class=\"form-group\">\n                                <label for=\"exampleInputPassword1\">Addresse</label>\n                                <input type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\">\n                            </div>\n                            <div class=\"form-group form-check\">\n                                <input type=\"checkbox\" class=\"form-check-input\" id=\"exampleCheck1\">\n                                <label class=\"form-check-label\" for=\"exampleCheck1\">Check me out</label>\n                            </div>\n                            <button type=\"submit\" mat-raised-button class=\"searchbtn\">register</button>\n                        </form>\n                    </ng-template>\n                </li>\n                <li [ngbNavItem]=\"2\">\n                    <a ngbNavLink>Log in</a>\n                    <ng-template ngbNavContent>\n                        <form>\n                            <div class=\"form-group\">\n                                <label for=\"exampleInputEmail1\">Email address</label>\n                                <input type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\">\n                                <small id=\"emailHelp\" class=\"form-text text-muted\">We'll never share your email with anyone\n                                    else.</small>\n                            </div>\n                            <div class=\"form-group\">\n                                <label for=\"exampleInputPassword1\">Password</label>\n                                <input type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\">\n                            </div>\n                            <button type=\"submit\" mat-raised-button class=\"searchbtn\">register</button>\n                        </form>\n                    </ng-template>\n                </li>\n            </ul>\n            <div [ngbNavOutlet]=\"nav\" class=\"mt-2\"></div>\n\n\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./src/app/hoster/hoster-routing.module.ts":
/*!*************************************************!*\
  !*** ./src/app/hoster/hoster-routing.module.ts ***!
  \*************************************************/
/*! exports provided: HosterRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HosterRoutingModule", function() { return HosterRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login/login.component */ "./src/app/hoster/login/login.component.ts");
/* harmony import */ var _hoster_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./hoster.component */ "./src/app/hoster/hoster.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");





const routes = [
    {
        path: '', component: _hoster_component__WEBPACK_IMPORTED_MODULE_2__["HosterComponent"], children: [
            {
                path: 'outh', component: _login_login_component__WEBPACK_IMPORTED_MODULE_1__["LoginComponent"]
            },
            {
                path: '', redirectTo: 'outh', pathMatch: 'full'
            },
        ]
    }
];
let HosterRoutingModule = class HosterRoutingModule {
};
HosterRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
    })
], HosterRoutingModule);



/***/ }),

/***/ "./src/app/hoster/hoster.component.css":
/*!*********************************************!*\
  !*** ./src/app/hoster/hoster.component.css ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2hvc3Rlci9ob3N0ZXIuY29tcG9uZW50LmNzcyJ9 */");

/***/ }),

/***/ "./src/app/hoster/hoster.component.ts":
/*!********************************************!*\
  !*** ./src/app/hoster/hoster.component.ts ***!
  \********************************************/
/*! exports provided: HosterComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HosterComponent", function() { return HosterComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let HosterComponent = class HosterComponent {
    constructor() { }
    ngOnInit() {
    }
};
HosterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-hoster',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./hoster.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/hoster/hoster.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./hoster.component.css */ "./src/app/hoster/hoster.component.css")).default]
    })
], HosterComponent);



/***/ }),

/***/ "./src/app/hoster/hoster.module.ts":
/*!*****************************************!*\
  !*** ./src/app/hoster/hoster.module.ts ***!
  \*****************************************/
/*! exports provided: HosterModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HosterModule", function() { return HosterModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _app_material_app_material_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../app-material/app-material.module */ "./src/app/app-material/app-material.module.ts");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/hoster/login/login.component.ts");
/* harmony import */ var _hoster_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./hoster.component */ "./src/app/hoster/hoster.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _hoster_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./hoster-routing.module */ "./src/app/hoster/hoster-routing.module.ts");








let HosterModule = class HosterModule {
};
HosterModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["NgModule"])({
        declarations: [
            _hoster_component__WEBPACK_IMPORTED_MODULE_4__["HosterComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"]
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_6__["CommonModule"],
            _hoster_routing_module__WEBPACK_IMPORTED_MODULE_7__["HosterRoutingModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_2__["NgbModule"],
            _app_material_app_material_module__WEBPACK_IMPORTED_MODULE_1__["AppMaterialModule"]
        ]
    })
], HosterModule);



/***/ }),

/***/ "./src/app/hoster/login/login.component.css":
/*!**************************************************!*\
  !*** ./src/app/hoster/login/login.component.css ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".row{\r\n    margin: 0px;\r\n    padding: 0px;\r\n}\r\n.red{\r\nbackground-color: red;\r\n}\r\n.blue{\r\nbackground-color: blue;\r\n}\r\nimg {\r\n  width: 16rem !important;\r\n  height:auto !important;\r\n  margin-bottom: 40px;\r\n}\r\n.div1{\r\n  background-image: url('1.jpg');\r\n  height: 100vh; \r\n  background-position: center;\r\n  background-repeat: no-repeat;\r\n  background-size: cover;\r\n}\r\n.div2{\r\n    background-image: url('2.jpg');\r\n    height: 100vh; \r\n    background-position: center;\r\n    background-repeat: no-repeat;\r\n    background-size: cover;\r\n  }\r\n.div3{\r\n    background-image: url('3.jpg');\r\n    height: 100vh; \r\n    background-position: center;\r\n    background-repeat: no-repeat;\r\n    background-size: cover;\r\n  }\r\n.div4{\r\n    background-image: url('4.jpg');\r\n    height: 100vh; \r\n    background-position: center;\r\n    background-repeat: no-repeat;\r\n    background-size: cover;\r\n  }\r\n.div5{\r\n    background-image: url('5.jpg');\r\n    height: 100vh; \r\n    background-position: center;\r\n    background-repeat: no-repeat;\r\n    background-size: cover;\r\n  }\r\n.searchbtn{\r\n    background-color: #006a70;\r\n    font-family: myfont;\r\n    font-size: 14px;\r\n    color :white ;\r\n  }\r\ndiv {\r\n    font-family: myfont;\r\n    color: #484848 !important;\r\n  }\r\ninput{\r\n   font-family: myfont !important;\r\n   color :#484848 !important;\r\n   font-size: 10pt !important;\r\n }\r\nh3,p {\r\n   color :white !important;\r\n }\r\na:active{\r\n   color:rgb(255, 56, 92) !important;\r\n }\r\na:hover{\r\n  color:rgb(255, 56, 92) !important;\r\n}\r\n.active{\r\n  color:rgb(255, 56, 92) !important;\r\n  }\r\na{\r\n   color : #484848 !important;\r\n }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9zdGVyL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7SUFDSSxXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUNBO0FBQ0EscUJBQXFCO0FBQ3JCO0FBQ0E7QUFDQSxzQkFBc0I7QUFDdEI7QUFFQTtFQUNFLHVCQUF1QjtFQUN2QixzQkFBc0I7RUFDdEIsbUJBQW1CO0FBQ3JCO0FBQ0E7RUFDRSw4QkFBcUQ7RUFDckQsYUFBYTtFQUNiLDJCQUEyQjtFQUMzQiw0QkFBNEI7RUFDNUIsc0JBQXNCO0FBQ3hCO0FBQ0E7SUFDSSw4QkFBcUQ7SUFDckQsYUFBYTtJQUNiLDJCQUEyQjtJQUMzQiw0QkFBNEI7SUFDNUIsc0JBQXNCO0VBQ3hCO0FBQ0Y7SUFDSSw4QkFBcUQ7SUFDckQsYUFBYTtJQUNiLDJCQUEyQjtJQUMzQiw0QkFBNEI7SUFDNUIsc0JBQXNCO0VBQ3hCO0FBQ0E7SUFDRSw4QkFBcUQ7SUFDckQsYUFBYTtJQUNiLDJCQUEyQjtJQUMzQiw0QkFBNEI7SUFDNUIsc0JBQXNCO0VBQ3hCO0FBQ0E7SUFDRSw4QkFBcUQ7SUFDckQsYUFBYTtJQUNiLDJCQUEyQjtJQUMzQiw0QkFBNEI7SUFDNUIsc0JBQXNCO0VBQ3hCO0FBRUE7SUFDRSx5QkFBeUI7SUFDekIsbUJBQW1CO0lBQ25CLGVBQWU7SUFDZixhQUFhO0VBQ2Y7QUFHQTtJQUNFLG1CQUFtQjtJQUNuQix5QkFBeUI7RUFDM0I7QUFFRDtHQUNFLDhCQUE4QjtHQUM5Qix5QkFBeUI7R0FDekIsMEJBQTBCO0NBQzVCO0FBQ0E7R0FDRSx1QkFBdUI7Q0FDekI7QUFDQTtHQUNFLGlDQUFpQztDQUNuQztBQUNBO0VBQ0MsaUNBQWlDO0FBQ25DO0FBQ0M7RUFDQyxpQ0FBaUM7RUFDakM7QUFDRDtHQUNFLDBCQUEwQjtDQUM1QiIsImZpbGUiOiJzcmMvYXBwL2hvc3Rlci9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnJvd3tcclxuICAgIG1hcmdpbjogMHB4O1xyXG4gICAgcGFkZGluZzogMHB4O1xyXG59XHJcbi5yZWR7XHJcbmJhY2tncm91bmQtY29sb3I6IHJlZDtcclxufVxyXG4uYmx1ZXtcclxuYmFja2dyb3VuZC1jb2xvcjogYmx1ZTtcclxufVxyXG5cclxuaW1nIHtcclxuICB3aWR0aDogMTZyZW0gIWltcG9ydGFudDtcclxuICBoZWlnaHQ6YXV0byAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1ib3R0b206IDQwcHg7XHJcbn1cclxuLmRpdjF7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy8xLmpwZ1wiKTtcclxuICBoZWlnaHQ6IDEwMHZoOyBcclxuICBiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXI7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG59XHJcbi5kaXYye1xyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKFwiLi4vLi4vLi4vYXNzZXRzL2ltYWdlcy8yLmpwZ1wiKTtcclxuICAgIGhlaWdodDogMTAwdmg7IFxyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgfVxyXG4uZGl2M3tcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvMy5qcGdcIik7XHJcbiAgICBoZWlnaHQ6IDEwMHZoOyBcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIH1cclxuICAuZGl2NHtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvNC5qcGdcIik7XHJcbiAgICBoZWlnaHQ6IDEwMHZoOyBcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIH1cclxuICAuZGl2NXtcclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybChcIi4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvNS5qcGdcIik7XHJcbiAgICBoZWlnaHQ6IDEwMHZoOyBcclxuICAgIGJhY2tncm91bmQtcG9zaXRpb246IGNlbnRlcjtcclxuICAgIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbiAgICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIH1cclxuXHJcbiAgLnNlYXJjaGJ0bntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDZhNzA7XHJcbiAgICBmb250LWZhbWlseTogbXlmb250O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgY29sb3IgOndoaXRlIDtcclxuICB9XHJcbiAgXHJcbiAgXHJcbiAgZGl2IHtcclxuICAgIGZvbnQtZmFtaWx5OiBteWZvbnQ7XHJcbiAgICBjb2xvcjogIzQ4NDg0OCAhaW1wb3J0YW50O1xyXG4gIH1cclxuXHJcbiBpbnB1dHtcclxuICAgZm9udC1mYW1pbHk6IG15Zm9udCAhaW1wb3J0YW50O1xyXG4gICBjb2xvciA6IzQ4NDg0OCAhaW1wb3J0YW50O1xyXG4gICBmb250LXNpemU6IDEwcHQgIWltcG9ydGFudDtcclxuIH1cclxuIGgzLHAge1xyXG4gICBjb2xvciA6d2hpdGUgIWltcG9ydGFudDtcclxuIH1cclxuIGE6YWN0aXZle1xyXG4gICBjb2xvcjpyZ2IoMjU1LCA1NiwgOTIpICFpbXBvcnRhbnQ7XHJcbiB9XHJcbiBhOmhvdmVye1xyXG4gIGNvbG9yOnJnYigyNTUsIDU2LCA5MikgIWltcG9ydGFudDtcclxufVxyXG4gLmFjdGl2ZXtcclxuICBjb2xvcjpyZ2IoMjU1LCA1NiwgOTIpICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gYXtcclxuICAgY29sb3IgOiAjNDg0ODQ4ICFpbXBvcnRhbnQ7XHJcbiB9Il19 */");

/***/ }),

/***/ "./src/app/hoster/login/login.component.ts":
/*!*************************************************!*\
  !*** ./src/app/hoster/login/login.component.ts ***!
  \*************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let LoginComponent = class LoginComponent {
    constructor() {
        this.images = [944, 1011, 984].map((n) => `https://picsum.photos/id/${n}/900/500`);
    }
    ngOnInit() {
    }
};
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/hoster/login/login.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.component.css */ "./src/app/hoster/login/login.component.css")).default]
    })
], LoginComponent);



/***/ })

}]);
//# sourceMappingURL=hoster-hoster-module-es2015.js.map