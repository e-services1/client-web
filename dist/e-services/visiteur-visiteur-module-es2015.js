(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["visiteur-visiteur-module"],{

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/login/login.component.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/login/login.component.html ***!
  \*******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container-fuild\">\n  <form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\">\n    <div class=\"form-group\">\n      <label for=\"exampleInputEmail1\">Email address</label>\n      <input formControlName=\"email\" type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\"\n        aria-describedby=\"emailHelp\">\n      <small id=\"emailHelp\" class=\"form-text text-muted\">We'll never share your email with anyone else.</small>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"exampleInputPassword1\">Password</label>\n      <input formControlName=\"password\" type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\">\n    </div>\n    <div class=\"w-100 d-flex justify-content-center\">\n      <button type=\"submit\" mat-raised-button class=\"btn-primary w-75\" [disabled]=\"!loginForm.valid\">Log in</button>\n    </div>\n    <div>\n      <div *ngIf=\"isSubmited && isOuthError\" style=\"color: red; text-align:center;font-size:8pt;\">email or password is\n        incorrect </div>\n    </div>\n    <div>\n      <p class=\"form-text text-muted text-center forgot\" (click)=\"forgotpassword()\" >Forgot password ?</p>\n    </div>\n  </form>\n  <hr *ngIf=\"!showLoader\"  class=\"clearfix\">\n  <mat-progress-bar *ngIf=\"showLoader\" mode=\"indeterminate\" style=\"margin: 1rem 0 1rem 0;\"></mat-progress-bar>\n  <small class=\"form-text text-muted\">Haven't account yet ? register or login with those social media</small>\n  <ul class=\"list-unstyled list-inline d-flex justify-content-center\">\n    <li class=\"list-inline-item\">\n      <a class=\"btn-floating btn-fb mx-1\">\n        <i class=\"fab fa-facebook-f fa-lg\" (click)=\"loginWithFacebook()\"  > </i>\n      </a>\n    </li>\n    <li class=\"list-inline-item\">\n      <a class=\"btn-floating btn-tw mx-1\">\n        <i class=\"fab fa-twitter fa-lg\"> </i>\n      </a>\n    </li>\n    <li class=\"list-inline-item\">\n      <a class=\"btn-floating btn-dribbble mx-1\">\n        <i class=\"fab fa-instagram fa-lg\"></i>\n      </a>\n    </li>\n  </ul>\n\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/navbar/navbar.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/navbar/navbar.component.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<nav class=\"navbar sticky-top navbar-expand-md navbar-light bg-light\">\n    <a [routerLink]=\"['/visiteur']\" class=\"navbar-brand\">\n         <img class=\"logo-navbar\" src=\"../../assets/logo/Khomsa logo-02.png\"></a>\n    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#navbarNav\"\n        aria-controls=\"navbarNav\" aria-expanded=\"false\" aria-label=\"Toggle navigation\">\n        <span class=\"navbar-toggler-icon\"></span>\n    </button>\n    <div class=\"collapse navbar-collapse\" id=\"navbarNav\">\n        <div class=\"navbar-nav w-100 d-flex flex-row-reverse bd-highlight\">\n            <ul class=\"navbar-nav\">\n                <li class=\"nav-item item\">\n                    <a class=\"nav-link\"matTooltip=\"want to offer somethings ?\"\n                    aria-label=\"BecomeDealer\"(click)=\"gotoHoster()\" >Become dealer</a>\n                </li>\n                <li class=\"nav-item item\">\n                    <a class=\"nav-link\" (click)=\"openHelp()\">Help</a>\n                </li>\n                <li class=\"nav-item item\">\n                    <a class=\"nav-link\" (click)=\"navigateTo('footer')\">About</a>\n                </li>\n                <li class=\"nav-item item\">\n                    <a class=\"nav-link\" (click)=\"Signup()\" >Sign up</a>\n                </li>\n                <li class=\"nav-item item\">\n                    <a class=\"nav-link\" (click)=\"Login()\" >Log in</a>\n                </li>\n            </ul>\n        </div>\n    </div>\n</nav>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/navbar/snackBar.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/navbar/snackBar.html ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"title\" style=\"font-family: myfont; font-size: 14px; color: #484848;\">\r\n    for more detail about user guid,\r\n    <button mat-button><a href=\"google.com\" style=\"color:#006a70\">clic here</a></button>\r\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/search-request/search-request.component.html":
/*!*************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/search-request/search-request.component.html ***!
  \*************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container-fuild main\">\n\n    <div class=\"bg\"></div>\n\n    <div class=\"row cardd d-flex justify-content-between align-items-end\">\n\n        <div class=\"col-xl-3 col-lg-5 col-md-6 col-sm-12 col-12  d-flex align-items-center justify-content-center \"\n            style=\"height: 100vh;\">\n            <form [formGroup]=\"searchFrom\" (ngSubmit)=\"onSubmit()\">\n                <mat-card class=\"example-card\">\n                    <mat-card-header>\n                        <mat-card-title class=\"display1\">Book unique places to</mat-card-title>\n                        <mat-card-subtitle class=\"display2\">stay, adventure and things to do</mat-card-subtitle>\n                    </mat-card-header>\n                    <mat-card-content>\n                        <div class=\"form-group\">\n                            <label for=\"text1\" class=\"display3\">Anything</label>\n                            <input formControlName=\"productName\" type=\"text\" class=\"form-control\" id=\"text1\"\n                                placeholder=\"you looking for\">\n                        </div>\n                        <div class=\"form-group\">\n                            <label for=\"exampleFormControlSelect1\" class=\"display3\">Select categorie</label>\n                            <select formControlName=\"productCategory\" (change)=\"onChangeCategory($event)\"\n                                class=\"form-control\" id=\"exampleFormControlSelect1\">\n                                <option value=\"\">Choose your category</option>\n                                <option *ngFor=\"let cat of category\" value={{cat}}>{{cat}}</option>\n                            </select>\n                            <div class=\"errormsg\" *ngIf=\"isSubmited && searchFrom.invalid\">\n                                <sup>*</sup>Make sure you have a valid request\n                            </div>\n                        </div>\n                    </mat-card-content>\n                    <mat-card-actions class=\"w-100 d-flex justify-content-end\">\n                        <button type=\"submit\" mat-raised-button class=\"searchbtn\">Explore</button>\n                    </mat-card-actions>\n                </mat-card>\n            </form>\n        </div>\n\n        <div class=\"col-4 d-flex justify-content-end\">\n            <div class=\"d-none d-md-block\">\n                <button mat-raised-button matTooltip=\"Check our policy terms\" aria-label=\"Terms\" class=\"terms\"\n                    (click)=\"openButtomSheet()\">\n                    <i class=\"fas fa-cog\"></i>\n                    Terms</button>\n            </div>\n        </div>\n\n    </div>\n\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/search-request/terms.html":
/*!******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/search-request/terms.html ***!
  \******************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<mat-list>\r\n    <a href=\"https://keep.google.com/\" mat-list-item (click)=\"openLink($event)\">\r\n      <span mat-line>Google Keep</span>\r\n      <span mat-line>Add to a note</span>\r\n    </a>\r\n  \r\n    <a href=\"https://docs.google.com/\" mat-list-item (click)=\"openLink($event)\">\r\n      <span mat-line>Google Docs</span>\r\n      <span mat-line>Embed in a document</span>\r\n    </a>\r\n  \r\n    <a href=\"https://plus.google.com/\" mat-list-item (click)=\"openLink($event)\">\r\n      <span mat-line>Google Plus</span>\r\n      <span mat-line>Share with your friends</span>\r\n    </a>\r\n  \r\n    <a href=\"https://hangouts.google.com/\" mat-list-item (click)=\"openLink($event)\">\r\n      <span mat-line>Google Hangouts</span>\r\n      <span mat-line>Show to your coworkers</span>\r\n    </a>\r\n  </mat-list>\r\n  ");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/search-response/search-response.component.html":
/*!***************************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/search-response/search-response.component.html ***!
  \***************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container-fuild w-100 \" style=\"margin: 0px;margin-top:8rem;\">\n    <div class=\"row d-flex justify-content-center\">\n        <div class=\"col-11\" style=\"padding:0px; \">\n            <!---->\n            <div class=\"row\">\n                <div class=\"col-12 d-flex justify-content-start\">\n                    <h1>300+ {{msg}}</h1>\n                </div>\n            </div>\n\n            <!---->\n\n            <!---->\n\n            <div class=\"row d-flex justify-content-between\" style=\"padding: 0px;\">\n                <div class=\"card mycard col-xl-3 col-lg-4 col-md-4 col-sm-6 col-12\" *ngFor=\"let item of list\">\n                    <!-- -->\n                    <ngb-carousel data-interval=\"false\">\n                        <ng-template ngbSlide *ngFor=\"let pic of item.urlPhotos\" >\n                            <div class=\"picsum-img-wrapper\">\n                                <img [src]=\"url+pic\" class=\"responsive\" alt=\"Random first slide\">\n                            </div>\n                            <div class=\"carousel-caption\">\n                                <h3>{{item.title}}</h3>\n                            </div>\n                        </ng-template>\n                    </ngb-carousel>\n\n                    <!---->\n\n                    <div (click)=\"alertOuth()\"  class=\"card-body\">\n                        <div class=\"row w-100 d-flex justify-content-between\">\n                            <div class=\"col-6 title mycol\">\n                                {{item.title}}\n                            </div>\n                            <div class=\"col-6 d-flex justify-content-end mycol\">\n                                <div><i class=\"fas fa-heart\" style=\"color: #006a70;\"></i>\n                                    {{item.like}}\n                                </div>\n                            </div>\n                        </div>\n                        <div class=\"row w-100\">\n                            <div class=\"col-12 detail mycol\">{{item.description}}</div>\n                        </div>\n                        <div class=\"row w-100\">\n                            <div class=\"col-12 cost mycol\">\n                                {{item.cost}}\n                                <i class=\"fas fa-dollar-sign\"></i>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <!---->\n        </div>\n    </div>\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/signup/signup.component.html":
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/signup/signup.component.html ***!
  \*********************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container-fuild\">\n  <form [formGroup]=\"signupform\" (ngSubmit)=\"onSubmit()\">\n    <div class=\"form-group\">\n      <label for=\"exampleInputEmail1\">Email address</label>\n      <input formControlName=\"email\" type=\"email\" class=\"form-control\" id=\"exampleInputEmail1\" aria-describedby=\"emailHelp\">\n      <small id=\"emailHelp\" class=\"form-text text-muted\">We'll never share your email with anyone else.</small>\n    </div>\n    <div class=\"form-group\">\n      <label for=\"exampleInputPassword1\">Password</label>\n      <input formControlName=\"password\" type=\"password\" class=\"form-control\" id=\"exampleInputPassword1\">\n    </div>\n    <div class=\"form-group\">\n      <label for=\"exampleInputPassword2\">Confirm password</label>\n      <input formControlName=\"confirmpassword\" type=\"password\" class=\"form-control\" id=\"exampleInputPassword2\">\n    </div>\n    <div class=\"form-group\">\n      <label for=\"username\">name</label>\n      <div class=\"row\">\n        <div class=\"col\">\n          <input formControlName=\"firstname\" type=\"text\" placeholder=\"first name\" class=\"form-control\" id=\"username\">\n        </div>\n        <div class=\"col\">\n          <input formControlName=\"lastname\" type=\"text\" placeholder=\"last name\" class=\"form-control\" id=\"username\">\n        </div>\n      </div>\n    </div>\n    <div class=\"w-100 d-flex justify-content-center\">\n      <button type=\"submit\" [disabled]=\"!signupform.valid || isSubmited\" mat-raised-button class=\"btn-primary w-75\">Sign up</button>\n    </div>\n    <div>\n      <div *ngIf=\"isCreatedError\" style=\"color: red; text-align:center;font-size:8pt;\">{{msgError}}</div>\n    </div>\n  </form>\n\n  <hr *ngIf=\"!showLoader\"  class=\"clearfix\">\n  <mat-progress-bar *ngIf=\"showLoader\" mode=\"indeterminate\" style=\"margin: 1rem 0 1rem 0;\"></mat-progress-bar>\n\n  <small class=\"form-text text-muted text-center\">You don't want to register with email ? try to signup with those social media</small>\n  <ul class=\"list-unstyled list-inline d-flex justify-content-center\">\n    <li class=\"list-inline-item\">\n      <a class=\"btn-floating btn-fb mx-1\">\n        <i class=\"fab fa-facebook-f fa-lg\"> </i>\n      </a>\n    </li>\n    <li class=\"list-inline-item\">\n      <a class=\"btn-floating btn-tw mx-1\">\n        <i class=\"fab fa-twitter fa-lg\"> </i>\n      </a>\n    </li>\n    <li class=\"list-inline-item\">\n      <a class=\"btn-floating btn-dribbble mx-1\">\n        <i class=\"fab fa-instagram fa-lg\"></i>\n      </a>\n    </li>\n  </ul>\n\n</div>");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/visiteur.component.html":
/*!****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/visiteur.component.html ***!
  \****************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<div class=\"container-fuild\">\r\n    <div class=\"main\">\r\n        <div class=\"mynav\">\r\n            <app-navbar (Navigate)=\"navigateTo($event)\"></app-navbar>\r\n            <mat-progress-bar\r\n             mode=\"indeterminate\" \r\n             *ngIf=\"showLoader\" \r\n             style=\"position: fixed;margin-top: 55.6;z-index: 99999;\"></mat-progress-bar>\r\n        </div>\r\n        <div class=\"router\">\r\n            <router-outlet></router-outlet>\r\n            <div #footer>\r\n                <app-explore></app-explore>\r\n                <app-footer></app-footer>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n");

/***/ }),

/***/ "./src/app/entity/user.ts":
/*!********************************!*\
  !*** ./src/app/entity/user.ts ***!
  \********************************/
/*! exports provided: User */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "User", function() { return User; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");

class User {
}


/***/ }),

/***/ "./src/app/services/modal.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/modal.service.ts ***!
  \*******************************************/
/*! exports provided: ModalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalService", function() { return ModalService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm2015/index.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");



let ModalService = class ModalService {
    constructor() {
        this.loginSubject = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](false);
        this.currentLogin = this.loginSubject.asObservable();
    }
    showLogin() {
        this.loginSubject.next(true);
    }
    hideLogin() {
        this.loginSubject.next(false);
    }
};
ModalService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Injectable"])({
        providedIn: 'root'
    })
], ModalService);



/***/ }),

/***/ "./src/app/visiteur/login/login.component.css":
/*!****************************************************!*\
  !*** ./src/app/visiteur/login/login.component.css ***!
  \****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ul>li {\r\n    padding-right: 10px;\r\n    color: #484848;\r\n}\r\ndiv,form,li,button {\r\n    font-family: myfont;\r\n    font-size: 14px ;\r\n    color: #484848;\r\n}\r\nbutton{\r\n    color: white;\r\n}\r\n.forgot {\r\n    font-size: 12px;\r\n    letter-spacing: 2px;\r\n    color: #484848;\r\n    margin-top:10px;\r\n    font-family: myfont;\r\n}\r\nul{\r\n    margin-top: 10px;\r\n}\r\ninput{\r\n    font-family: 'Heebo', sans-serif;\r\n    font-size: 14px;\r\n}\r\n.form-control:focus{\r\n    border-color: #006a70;\r\n    box-shadow: 0 0 0 0.2rem rgba(0, 106, 112, 0.25);\r\n    }\r\nbutton{\r\n    background-color: #006a70;\r\n}\r\nbutton:focus {\r\n    outline: none !important ;\r\n    box-shadow:none !important;\r\n  }\r\n::ng-deep .mat-progress-bar-fill::after {\r\n    background-color:#006a70;\r\n}\r\n::ng-deep .mat-progress-bar-buffer {\r\n    background: #50bac0;\r\n}\r\n::ng-deep .mat-progress-bar {\r\n    border-radius: 2px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlzaXRldXIvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLG1CQUFtQjtJQUNuQixjQUFjO0FBQ2xCO0FBQ0E7SUFDSSxtQkFBbUI7SUFDbkIsZ0JBQWdCO0lBQ2hCLGNBQWM7QUFDbEI7QUFFQTtJQUNJLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGVBQWU7SUFDZixtQkFBbUI7SUFDbkIsY0FBYztJQUNkLGVBQWU7SUFDZixtQkFBbUI7QUFDdkI7QUFDQTtJQUNJLGdCQUFnQjtBQUNwQjtBQUNBO0lBQ0ksZ0NBQWdDO0lBQ2hDLGVBQWU7QUFDbkI7QUFDQTtJQUNJLHFCQUFxQjtJQUNyQixnREFBZ0Q7SUFDaEQ7QUFDSjtJQUNJLHlCQUF5QjtBQUM3QjtBQUNBO0lBQ0kseUJBQXlCO0lBQ3pCLDBCQUEwQjtFQUM1QjtBQUNBO0lBQ0Usd0JBQXdCO0FBQzVCO0FBRUE7SUFDSSxtQkFBbUI7QUFDdkI7QUFFQTtJQUNJLGtCQUFrQjtBQUN0QiIsImZpbGUiOiJzcmMvYXBwL3Zpc2l0ZXVyL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJ1bD5saSB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG4gICAgY29sb3I6ICM0ODQ4NDg7XHJcbn1cclxuZGl2LGZvcm0sbGksYnV0dG9uIHtcclxuICAgIGZvbnQtZmFtaWx5OiBteWZvbnQ7XHJcbiAgICBmb250LXNpemU6IDE0cHggO1xyXG4gICAgY29sb3I6ICM0ODQ4NDg7XHJcbn1cclxuXHJcbmJ1dHRvbntcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxufVxyXG4uZm9yZ290IHtcclxuICAgIGZvbnQtc2l6ZTogMTJweDtcclxuICAgIGxldHRlci1zcGFjaW5nOiAycHg7XHJcbiAgICBjb2xvcjogIzQ4NDg0ODtcclxuICAgIG1hcmdpbi10b3A6MTBweDtcclxuICAgIGZvbnQtZmFtaWx5OiBteWZvbnQ7XHJcbn1cclxudWx7XHJcbiAgICBtYXJnaW4tdG9wOiAxMHB4O1xyXG59XHJcbmlucHV0e1xyXG4gICAgZm9udC1mYW1pbHk6ICdIZWVibycsIHNhbnMtc2VyaWY7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbn1cclxuLmZvcm0tY29udHJvbDpmb2N1c3tcclxuICAgIGJvcmRlci1jb2xvcjogIzAwNmE3MDtcclxuICAgIGJveC1zaGFkb3c6IDAgMCAwIDAuMnJlbSByZ2JhKDAsIDEwNiwgMTEyLCAwLjI1KTtcclxuICAgIH0gICBcclxuYnV0dG9ue1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzAwNmE3MDtcclxufVxyXG5idXR0b246Zm9jdXMge1xyXG4gICAgb3V0bGluZTogbm9uZSAhaW1wb3J0YW50IDtcclxuICAgIGJveC1zaGFkb3c6bm9uZSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICA6Om5nLWRlZXAgLm1hdC1wcm9ncmVzcy1iYXItZmlsbDo6YWZ0ZXIge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjojMDA2YTcwO1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC1wcm9ncmVzcy1iYXItYnVmZmVyIHtcclxuICAgIGJhY2tncm91bmQ6ICM1MGJhYzA7XHJcbn1cclxuXHJcbjo6bmctZGVlcCAubWF0LXByb2dyZXNzLWJhciB7XHJcbiAgICBib3JkZXItcmFkaXVzOiAycHg7XHJcbn0iXX0= */");

/***/ }),

/***/ "./src/app/visiteur/login/login.component.ts":
/*!***************************************************!*\
  !*** ./src/app/visiteur/login/login.component.ts ***!
  \***************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_modal_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/modal.service */ "./src/app/services/modal.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_client_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../services/client/authentication.service */ "./src/app/services/client/authentication.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");







let LoginComponent = class LoginComponent {
    constructor(formBuilder, authenticationService, modalService, zone, router) {
        this.formBuilder = formBuilder;
        this.authenticationService = authenticationService;
        this.modalService = modalService;
        this.zone = zone;
        this.router = router;
        this.isSubmited = false;
        this.showLoader = false;
    }
    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].email]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_6__["Validators"].minLength(3)]),
        });
    }
    forgotpassword() {
        this.modalService.hideLogin();
        this.router.navigate(['forgotpassword/request']);
    }
    onSubmit() {
        //  this.submitted = true;
        // stop here if form is invalid
        /* if (this.loginForm.invalid) {
             return;
         }*/
        //  this.loading = true;
        this.isSubmited = true;
        this.showLoader = true;
        this.authenticationService.login(this.loginForm.value.email, this.loginForm.value.password)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])())
            .subscribe(data => {
            if (data.status == 200) {
                this.modalService.hideLogin();
                this.router.navigate(['/client']);
                this.isOuthError = false;
                this.showLoader = false;
            }
        }, error => {
            console.error(error);
            this.isOuthError = true;
            this.showLoader = false;
        });
    }
    loginWithFacebook() {
        var my = this;
        this.isSubmited = true;
        this.showLoader = true;
        login().login(function (response) {
            if (response.authResponse) {
                console.log(response.authResponse);
                let token = response.authResponse.accessToken;
                let id = response.authResponse.userID;
                my.zone.run(() => {
                    my.authenticationService.loginWithFacebook(id, token)
                        .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_5__["first"])())
                        .subscribe(data => {
                        if (data.status == 200) {
                            my.modalService.hideLogin();
                            my.router.navigate(['/client']);
                            my.isOuthError = false;
                            my.showLoader = false;
                        }
                    }, error => {
                        console.error(error);
                        my.isOuthError = true;
                        my.showLoader = false;
                    });
                });
            }
            else {
                console.log('User cancelled login or did not fully authorize.');
            }
        }, {
            scope: 'email',
            return_scopes: true
        }, {
            scope: 'photos',
            return_scopes: true
        });
    }
};
LoginComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"] },
    { type: _services_client_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] },
    { type: _services_modal_service__WEBPACK_IMPORTED_MODULE_1__["ModalService"] },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__["NgZone"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-login',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./login.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/login/login.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./login.component.css */ "./src/app/visiteur/login/login.component.css")).default]
    })
], LoginComponent);



/***/ }),

/***/ "./src/app/visiteur/navbar/navbar.component.css":
/*!******************************************************!*\
  !*** ./src/app/visiteur/navbar/navbar.component.css ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".item{\r\n    margin-right: 5px ;\r\n}\r\nnav{\r\n    font-family: myfont !important;\r\n    font-size: 14px !important;\r\n}\r\nnav .navbar-nav li a{\r\n    color:#484848;\r\n    }\r\n::ng-deep nav .navbar-nav li a:hover{\r\n        color:#767676 !important;\r\n}\r\nimg.logo-navbar {\r\n    width: 8rem !important;\r\n    height:auto !important;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlzaXRldXIvbmF2YmFyL25hdmJhci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0FBQ3RCO0FBQ0E7SUFDSSw4QkFBOEI7SUFDOUIsMEJBQTBCO0FBQzlCO0FBQ0E7SUFDSSxhQUFhO0lBQ2I7QUFDSjtRQUNRLHdCQUF3QjtBQUNoQztBQUNBO0lBQ0ksc0JBQXNCO0lBQ3RCLHNCQUFzQjtBQUMxQiIsImZpbGUiOiJzcmMvYXBwL3Zpc2l0ZXVyL25hdmJhci9uYXZiYXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pdGVte1xyXG4gICAgbWFyZ2luLXJpZ2h0OiA1cHggO1xyXG59XHJcbm5hdntcclxuICAgIGZvbnQtZmFtaWx5OiBteWZvbnQgIWltcG9ydGFudDtcclxuICAgIGZvbnQtc2l6ZTogMTRweCAhaW1wb3J0YW50O1xyXG59XHJcbm5hdiAubmF2YmFyLW5hdiBsaSBhe1xyXG4gICAgY29sb3I6IzQ4NDg0ODtcclxuICAgIH1cclxuOjpuZy1kZWVwIG5hdiAubmF2YmFyLW5hdiBsaSBhOmhvdmVye1xyXG4gICAgICAgIGNvbG9yOiM3Njc2NzYgIWltcG9ydGFudDtcclxufVxyXG5pbWcubG9nby1uYXZiYXIge1xyXG4gICAgd2lkdGg6IDhyZW0gIWltcG9ydGFudDtcclxuICAgIGhlaWdodDphdXRvICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ "./src/app/visiteur/navbar/navbar.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/visiteur/navbar/navbar.component.ts ***!
  \*****************************************************/
/*! exports provided: NavbarComponent, HelpSnackBar */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavbarComponent", function() { return NavbarComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HelpSnackBar", function() { return HelpSnackBar; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_modal_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/modal.service */ "./src/app/services/modal.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_client_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../services/client/authentication.service */ "./src/app/services/client/authentication.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../login/login.component */ "./src/app/visiteur/login/login.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../signup/signup.component */ "./src/app/visiteur/signup/signup.component.ts");








let NavbarComponent = class NavbarComponent {
    constructor(dialog, snackBar, router, currentUser, modalService) {
        this.dialog = dialog;
        this.snackBar = snackBar;
        this.router = router;
        this.currentUser = currentUser;
        this.modalService = modalService;
        this.Navigate = new _angular_core__WEBPACK_IMPORTED_MODULE_4__["EventEmitter"]();
        this.dialogState = 0;
    }
    navigateTo(element) {
        this.Navigate.emit(element);
    }
    Login() {
        if (this.dialogState == 0) {
            const dialogRef = this.dialog.open(_login_login_component__WEBPACK_IMPORTED_MODULE_6__["LoginComponent"], {});
            dialogRef.afterOpen().subscribe(rslt => {
                this.dialogState = 1;
                this.modalService.showLogin();
            });
            dialogRef.afterClosed().subscribe(result => {
                this.dialogState = 0;
                this.modalService.hideLogin();
            });
        }
    }
    Signup() {
        if (this.dialogState == 0) {
            const dialogRef = this.dialog.open(_signup_signup_component__WEBPACK_IMPORTED_MODULE_7__["SignupComponent"], {});
            dialogRef.afterOpen().subscribe(rslt => {
                this.dialogState = 1;
            });
            dialogRef.afterClosed().subscribe(result => {
                this.dialogState = 0;
            });
        }
    }
    openHelp() {
        this.snackBar.openFromComponent(HelpSnackBar, {
            duration: 5000,
            panelClass: ['Helpsnackbar']
        });
    }
    ngOnInit() {
        /*this.currentUser.currentUser.subscribe((data :OuthResponse) => {
          if (data!=null){
            if(data.token!=null){
              this.dialog.closeAll();
            }
          }
        });*/
        this.modalService.currentLogin.subscribe(data => {
            if (!data) {
                this.dialog.closeAll();
            }
        });
    }
    gotoHoster() {
        this.router.navigate(['hoster']);
    }
};
NavbarComponent.ctorParameters = () => [
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatDialog"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_client_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"] },
    { type: _services_modal_service__WEBPACK_IMPORTED_MODULE_1__["ModalService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Output"])()
], NavbarComponent.prototype, "Navigate", void 0);
NavbarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'app-navbar',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./navbar.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/navbar/navbar.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./navbar.component.css */ "./src/app/visiteur/navbar/navbar.component.css")).default]
    })
], NavbarComponent);

let HelpSnackBar = class HelpSnackBar {
};
HelpSnackBar = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["Component"])({
        selector: 'Help',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./snackBar.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/navbar/snackBar.html")).default,
    })
], HelpSnackBar);



/***/ }),

/***/ "./src/app/visiteur/search-request/search-request.component.css":
/*!**********************************************************************!*\
  !*** ./src/app/visiteur/search-request/search-request.component.css ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".bg {\r\n    width: 100%;\r\n    height: 100vh;\r\n    background-image: url(\"https://source.unsplash.com/random/1600x700/?artisanat\"); \r\n    background-position: center;\r\n    background-repeat: no-repeat;\r\n    background-size: cover;\r\n    background-color: rgb(233, 233, 233);\r\n    background-attachment: fixed;\r\n  }\r\n\r\n\r\n.main{\r\n  display: grid;\r\n}\r\n\r\n\r\n.bg,.cardd{\r\n  grid-area: 1/1;\r\n}\r\n\r\n\r\n.row{\r\n  margin: 0px;\r\n  padding:0px;\r\n}\r\n\r\n\r\n.vertical-center {\r\n  top: 25%;\r\n  -webkit-transform: translateY(-50%);\r\n          transform: translateY(-50%);\r\n}\r\n\r\n\r\n.terms{\r\n  margin-bottom: 10px;\r\n  font-family: myfont;\r\n  font-size: 14px;\r\n  letter-spacing: 1px;\r\n}\r\n\r\n\r\n.searchbtn{\r\n  background-color: #006a70;\r\n  font-family: myfont;\r\n  font-size: 14px;\r\n  color :white ;\r\n}\r\n\r\n\r\n.display1,.display2{\r\n  font-family: myfont;\r\n}\r\n\r\n\r\n.display1{\r\n  font-size: 27px;\r\n  color: #484848;\r\n}\r\n\r\n\r\n.display2{\r\n  color:#767676 ;\r\n  font-size: 18px;\r\n}\r\n\r\n\r\n.row{\r\n  margin: 0px;\r\n  padding: 0px;\r\n}\r\n\r\n\r\n.form-control:focus{\r\n  border-color: #006a70;\r\n  box-shadow: 0 0 0 0.2rem rgba(0, 106, 112, 0.25);\r\n  }\r\n\r\n\r\n.display3{\r\n  font-size: 14px;\r\n  font-family: myfont;\r\n}\r\n\r\n\r\nselect{\r\n  font-size: 14px;\r\n  font-family: myfont;\r\n}\r\n\r\n\r\ninput{\r\n  font-family: 'Heebo', sans-serif;\r\n  font-size: 14px;\r\n}\r\n\r\n\r\ninput::-webkit-input-placeholder{\r\n  font-size: 14px;\r\n  font-family: myfont;\r\n}\r\n\r\n\r\ninput::-moz-placeholder{\r\n  font-size: 14px;\r\n  font-family: myfont;\r\n}\r\n\r\n\r\ninput::-ms-input-placeholder{\r\n  font-size: 14px;\r\n  font-family: myfont;\r\n}\r\n\r\n\r\ninput::placeholder{\r\n  font-size: 14px;\r\n  font-family: myfont;\r\n}\r\n\r\n\r\n.black{\r\n  background-color: black;\r\n}\r\n\r\n\r\n.green{\r\n  background-color: green;\r\n}\r\n\r\n\r\n.red{\r\n  background-color: red;\r\n}\r\n\r\n\r\n.errormsg{\r\n  font-size: 8pt;\r\n  color:red ;\r\n  margin: 5px 0px 0px 0px;\r\n  padding: 0px ;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlzaXRldXIvc2VhcmNoLXJlcXVlc3Qvc2VhcmNoLXJlcXVlc3QuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFdBQVc7SUFDWCxhQUFhO0lBQ2IsK0VBQStFO0lBQy9FLDJCQUEyQjtJQUMzQiw0QkFBNEI7SUFDNUIsc0JBQXNCO0lBQ3RCLG9DQUFvQztJQUNwQyw0QkFBNEI7RUFDOUI7OztBQUdGO0VBQ0UsYUFBYTtBQUNmOzs7QUFDQTtFQUNFLGNBQWM7QUFDaEI7OztBQUVBO0VBQ0UsV0FBVztFQUNYLFdBQVc7QUFDYjs7O0FBRUE7RUFDRSxRQUFRO0VBRVIsbUNBQTJCO1VBQTNCLDJCQUEyQjtBQUM3Qjs7O0FBRUE7RUFDRSxtQkFBbUI7RUFDbkIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixtQkFBbUI7QUFDckI7OztBQUVBO0VBQ0UseUJBQXlCO0VBQ3pCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsYUFBYTtBQUNmOzs7QUFDQTtFQUNFLG1CQUFtQjtBQUNyQjs7O0FBQ0E7RUFDRSxlQUFlO0VBQ2YsY0FBYztBQUNoQjs7O0FBQ0E7RUFDRSxjQUFjO0VBQ2QsZUFBZTtBQUNqQjs7O0FBQ0E7RUFDRSxXQUFXO0VBQ1gsWUFBWTtBQUNkOzs7QUFFQTtFQUNFLHFCQUFxQjtFQUNyQixnREFBZ0Q7RUFDaEQ7OztBQUVGO0VBQ0UsZUFBZTtFQUNmLG1CQUFtQjtBQUNyQjs7O0FBQ0E7RUFDRSxlQUFlO0VBQ2YsbUJBQW1CO0FBQ3JCOzs7QUFDQTtFQUNFLGdDQUFnQztFQUNoQyxlQUFlO0FBQ2pCOzs7QUFDQTtFQUNFLGVBQWU7RUFDZixtQkFBbUI7QUFDckI7OztBQUhBO0VBQ0UsZUFBZTtFQUNmLG1CQUFtQjtBQUNyQjs7O0FBSEE7RUFDRSxlQUFlO0VBQ2YsbUJBQW1CO0FBQ3JCOzs7QUFIQTtFQUNFLGVBQWU7RUFDZixtQkFBbUI7QUFDckI7OztBQUNBO0VBQ0UsdUJBQXVCO0FBQ3pCOzs7QUFDQTtFQUNFLHVCQUF1QjtBQUN6Qjs7O0FBRUE7RUFDRSxxQkFBcUI7QUFDdkI7OztBQUNBO0VBQ0UsY0FBYztFQUNkLFVBQVU7RUFDVix1QkFBdUI7RUFDdkIsYUFBYTtBQUNmIiwiZmlsZSI6InNyYy9hcHAvdmlzaXRldXIvc2VhcmNoLXJlcXVlc3Qvc2VhcmNoLXJlcXVlc3QuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5iZyB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoXCJodHRwczovL3NvdXJjZS51bnNwbGFzaC5jb20vcmFuZG9tLzE2MDB4NzAwLz9hcnRpc2FuYXRcIik7IFxyXG4gICAgYmFja2dyb3VuZC1wb3NpdGlvbjogY2VudGVyO1xyXG4gICAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjMzLCAyMzMsIDIzMyk7XHJcbiAgICBiYWNrZ3JvdW5kLWF0dGFjaG1lbnQ6IGZpeGVkO1xyXG4gIH1cclxuXHJcblxyXG4ubWFpbntcclxuICBkaXNwbGF5OiBncmlkO1xyXG59XHJcbi5iZywuY2FyZGR7XHJcbiAgZ3JpZC1hcmVhOiAxLzE7XHJcbn1cclxuXHJcbi5yb3d7XHJcbiAgbWFyZ2luOiAwcHg7XHJcbiAgcGFkZGluZzowcHg7XHJcbn1cclxuXHJcbi52ZXJ0aWNhbC1jZW50ZXIge1xyXG4gIHRvcDogMjUlO1xyXG4gIC1tcy10cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XHJcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xyXG59XHJcblxyXG4udGVybXN7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICBmb250LWZhbWlseTogbXlmb250O1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBsZXR0ZXItc3BhY2luZzogMXB4O1xyXG59XHJcblxyXG4uc2VhcmNoYnRue1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDZhNzA7XHJcbiAgZm9udC1mYW1pbHk6IG15Zm9udDtcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgY29sb3IgOndoaXRlIDtcclxufVxyXG4uZGlzcGxheTEsLmRpc3BsYXkye1xyXG4gIGZvbnQtZmFtaWx5OiBteWZvbnQ7XHJcbn1cclxuLmRpc3BsYXkxe1xyXG4gIGZvbnQtc2l6ZTogMjdweDtcclxuICBjb2xvcjogIzQ4NDg0ODtcclxufVxyXG4uZGlzcGxheTJ7XHJcbiAgY29sb3I6Izc2NzY3NiA7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG59XHJcbi5yb3d7XHJcbiAgbWFyZ2luOiAwcHg7XHJcbiAgcGFkZGluZzogMHB4O1xyXG59XHJcblxyXG4uZm9ybS1jb250cm9sOmZvY3Vze1xyXG4gIGJvcmRlci1jb2xvcjogIzAwNmE3MDtcclxuICBib3gtc2hhZG93OiAwIDAgMCAwLjJyZW0gcmdiYSgwLCAxMDYsIDExMiwgMC4yNSk7XHJcbiAgfVxyXG4gIFxyXG4uZGlzcGxheTN7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGZvbnQtZmFtaWx5OiBteWZvbnQ7XHJcbn1cclxuc2VsZWN0e1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxuICBmb250LWZhbWlseTogbXlmb250O1xyXG59XHJcbmlucHV0e1xyXG4gIGZvbnQtZmFtaWx5OiAnSGVlYm8nLCBzYW5zLXNlcmlmO1xyXG4gIGZvbnQtc2l6ZTogMTRweDtcclxufVxyXG5pbnB1dDo6cGxhY2Vob2xkZXJ7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIGZvbnQtZmFtaWx5OiBteWZvbnQ7XHJcbn1cclxuLmJsYWNre1xyXG4gIGJhY2tncm91bmQtY29sb3I6IGJsYWNrO1xyXG59XHJcbi5ncmVlbntcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBncmVlbjtcclxufVxyXG5cclxuLnJlZHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZWQ7XHJcbn1cclxuLmVycm9ybXNne1xyXG4gIGZvbnQtc2l6ZTogOHB0O1xyXG4gIGNvbG9yOnJlZCA7XHJcbiAgbWFyZ2luOiA1cHggMHB4IDBweCAwcHg7XHJcbiAgcGFkZGluZzogMHB4IDtcclxufSJdfQ== */");

/***/ }),

/***/ "./src/app/visiteur/search-request/search-request.component.ts":
/*!*********************************************************************!*\
  !*** ./src/app/visiteur/search-request/search-request.component.ts ***!
  \*********************************************************************/
/*! exports provided: SearchRequestComponent, TermsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchRequestComponent", function() { return SearchRequestComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TermsComponent", function() { return TermsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var src_app_entity_category__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/entity/category */ "./src/app/entity/category.ts");






let SearchRequestComponent = class SearchRequestComponent {
    constructor(route, fb, _bottomSheet) {
        this.route = route;
        this.fb = fb;
        this._bottomSheet = _bottomSheet;
        this.category = Object.keys(src_app_entity_category__WEBPACK_IMPORTED_MODULE_5__["Category"]).filter(k => typeof src_app_entity_category__WEBPACK_IMPORTED_MODULE_5__["Category"][k] === "number");
        this.isSubmited = false;
    }
    ngOnInit() {
        this.searchFrom = this.fb.group({
            productName: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]],
            productCategory: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["Validators"].required]]
        });
    }
    onChangeCategory(event) {
        this.searchFrom.controls['productCategory'].setValue(event.target.value, {
            onlySelf: true
        });
    }
    onSubmit() {
        this.isSubmited = true;
        if (this.searchFrom.valid) {
            this.route.navigate(['visiteur/rep/' +
                    this.searchFrom.controls['productCategory'].value
                    + '/' + this.searchFrom.controls['productName'].value + '']);
        }
    }
    openButtomSheet() {
        this._bottomSheet.open(TermsComponent);
    }
};
SearchRequestComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatBottomSheet"] }
];
SearchRequestComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-search-request',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./search-request.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/search-request/search-request.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./search-request.component.css */ "./src/app/visiteur/search-request/search-request.component.css")).default]
    })
], SearchRequestComponent);

let TermsComponent = class TermsComponent {
    constructor(_bottomSheetRef) {
        this._bottomSheetRef = _bottomSheetRef;
    }
    openLink(event) {
        this._bottomSheetRef.dismiss();
        event.preventDefault();
    }
};
TermsComponent.ctorParameters = () => [
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_4__["MatBottomSheetRef"] }
];
TermsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'terms',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./terms.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/search-request/terms.html")).default,
    })
], TermsComponent);



/***/ }),

/***/ "./src/app/visiteur/search-response/search-response.component.css":
/*!************************************************************************!*\
  !*** ./src/app/visiteur/search-response/search-response.component.css ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".mycard{\r\n    padding:7px;\r\n}\r\n.row{\r\n    margin: 0px;\r\n    padding: 0px;\r\n}\r\n.IMGresponsive {\r\n    width: 100%;\r\n    height: auto;\r\n  }\r\n.card{\r\n    font-family:myfont;\r\n    color: #484848;\r\n    border: 0px solid;\r\n}\r\n.card-body{\r\n    padding: 5px;\r\n}\r\n.title{\r\n    font-size: 16px;\r\n}\r\n.detail{\r\n    color: #767676;\r\n    font-size: 12px;\r\n    font-family: 'Heebo', sans-serif;\r\n}\r\n.cost{\r\n    font-weight: bold;\r\n}\r\nh1{\r\n    font-family: myfont;\r\n    color: #484848;\r\n    font-size: 26px;\r\n    letter-spacing: 1px;\r\n}\r\n.mycol{\r\n    padding :0px;\r\n}\r\nimg{\r\n    height: 200px;\r\n}\r\n.carousel-indicators li {\r\n    width : 2px!important;\r\n  }\r\n.responsive {\r\n    width: 100%;\r\n    height: 200px;\r\n    position:inherit;\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlzaXRldXIvc2VhcmNoLXJlc3BvbnNlL3NlYXJjaC1yZXNwb25zZS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksV0FBVztBQUNmO0FBQ0E7SUFDSSxXQUFXO0lBQ1gsWUFBWTtBQUNoQjtBQUVBO0lBQ0ksV0FBVztJQUNYLFlBQVk7RUFDZDtBQUNGO0lBQ0ksa0JBQWtCO0lBQ2xCLGNBQWM7SUFDZCxpQkFBaUI7QUFDckI7QUFDQTtJQUNJLFlBQVk7QUFDaEI7QUFDQTtJQUNJLGVBQWU7QUFDbkI7QUFFQTtJQUNJLGNBQWM7SUFDZCxlQUFlO0lBQ2YsZ0NBQWdDO0FBQ3BDO0FBQ0E7SUFDSSxpQkFBaUI7QUFDckI7QUFDQTtJQUNJLG1CQUFtQjtJQUNuQixjQUFjO0lBQ2QsZUFBZTtJQUNmLG1CQUFtQjtBQUN2QjtBQUNBO0lBQ0ksWUFBWTtBQUNoQjtBQUNBO0lBQ0ksYUFBYTtBQUNqQjtBQUVBO0lBQ0kscUJBQXFCO0VBQ3ZCO0FBRUY7SUFDSSxXQUFXO0lBQ1gsYUFBYTtJQUNiLGdCQUFnQjtFQUNsQiIsImZpbGUiOiJzcmMvYXBwL3Zpc2l0ZXVyL3NlYXJjaC1yZXNwb25zZS9zZWFyY2gtcmVzcG9uc2UuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5teWNhcmR7XHJcbiAgICBwYWRkaW5nOjdweDtcclxufVxyXG4ucm93e1xyXG4gICAgbWFyZ2luOiAwcHg7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuXHJcbi5JTUdyZXNwb25zaXZlIHtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgaGVpZ2h0OiBhdXRvO1xyXG4gIH1cclxuLmNhcmR7XHJcbiAgICBmb250LWZhbWlseTpteWZvbnQ7XHJcbiAgICBjb2xvcjogIzQ4NDg0ODtcclxuICAgIGJvcmRlcjogMHB4IHNvbGlkO1xyXG59XHJcbi5jYXJkLWJvZHl7XHJcbiAgICBwYWRkaW5nOiA1cHg7XHJcbn1cclxuLnRpdGxle1xyXG4gICAgZm9udC1zaXplOiAxNnB4O1xyXG59XHJcblxyXG4uZGV0YWlse1xyXG4gICAgY29sb3I6ICM3Njc2NzY7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBmb250LWZhbWlseTogJ0hlZWJvJywgc2Fucy1zZXJpZjtcclxufVxyXG4uY29zdHtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbmgxe1xyXG4gICAgZm9udC1mYW1pbHk6IG15Zm9udDtcclxuICAgIGNvbG9yOiAjNDg0ODQ4O1xyXG4gICAgZm9udC1zaXplOiAyNnB4O1xyXG4gICAgbGV0dGVyLXNwYWNpbmc6IDFweDtcclxufVxyXG4ubXljb2x7XHJcbiAgICBwYWRkaW5nIDowcHg7XHJcbn1cclxuaW1ne1xyXG4gICAgaGVpZ2h0OiAyMDBweDtcclxufVxyXG5cclxuLmNhcm91c2VsLWluZGljYXRvcnMgbGkge1xyXG4gICAgd2lkdGggOiAycHghaW1wb3J0YW50O1xyXG4gIH1cclxuICBcclxuLnJlc3BvbnNpdmUge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBoZWlnaHQ6IDIwMHB4O1xyXG4gICAgcG9zaXRpb246aW5oZXJpdDtcclxuICB9Il19 */");

/***/ }),

/***/ "./src/app/visiteur/search-response/search-response.component.ts":
/*!***********************************************************************!*\
  !*** ./src/app/visiteur/search-response/search-response.component.ts ***!
  \***********************************************************************/
/*! exports provided: SearchResponseComponent, searchResponsesnackbar */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchResponseComponent", function() { return SearchResponseComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "searchResponsesnackbar", function() { return searchResponsesnackbar; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/loader.service */ "./src/app/services/loader.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");
/* harmony import */ var _entity_product__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../entity/product */ "./src/app/entity/product.ts");
/* harmony import */ var _services_product_product_story_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../services/product/product-story.service */ "./src/app/services/product/product-story.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");








let SearchResponseComponent = class SearchResponseComponent {
    constructor(activateRouter, productService, snackBar, loader) {
        this.activateRouter = activateRouter;
        this.productService = productService;
        this.snackBar = snackBar;
        this.loader = loader;
        this.url = "";
    }
    ngOnInit() {
        this.categorie = this.activateRouter.snapshot.params.categorie;
        this.subject = this.activateRouter.snapshot.params.subject;
        this.msg = "place to " + this.categorie;
        switch (this.categorie) {
            case "adventure": {
                this.url = src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].apiUrl + src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].media.adventure;
                break;
            }
            case "product": {
                this.url = src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].apiUrl + src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].media.product;
                break;
            }
            case "stay": {
                this.url = src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].apiUrl + src_environments_environment__WEBPACK_IMPORTED_MODULE_7__["environment"].media.stay;
                break;
            }
        }
        this.searchByName();
    }
    searchByName() {
        this.loader.show();
        this.list = new Array();
        this.productService.searchByCategory(0, 20, this.subject, this.categorie).subscribe(data => {
            let product;
            data.payload.forEach(element => {
                product = new _entity_product__WEBPACK_IMPORTED_MODULE_3__["Product"]();
                product = element;
                this.list.push(product);
            });
        }, err => {
            console.error(err);
        }, () => {
            this.loader.hide();
        });
    }
    alertOuth() {
        this.snackBar.openFromComponent(searchResponsesnackbar, {
            duration: 5 * 1000,
            panelClass: ['Helpsnackbar']
        });
    }
};
SearchResponseComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["ActivatedRoute"] },
    { type: _services_product_product_story_service__WEBPACK_IMPORTED_MODULE_4__["ProductStoryService"] },
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBar"] },
    { type: _services_loader_service__WEBPACK_IMPORTED_MODULE_1__["LoaderService"] }
];
SearchResponseComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["Component"])({
        selector: 'app-search-response',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./search-response.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/search-response/search-response.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./search-response.component.css */ "./src/app/visiteur/search-response/search-response.component.css")).default]
    })
], SearchResponseComponent);

let searchResponsesnackbar = class searchResponsesnackbar {
};
searchResponsesnackbar = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["Component"])({
        selector: 'snack-bar-component-example-snack',
        template: '<span class="style">you need to login before</span>',
        styles: ["\n    .style {\n      color: #484848;\n      font-family: myfont;\n      font-size: 14px;\n      text-align: center;\n    }\n  "]
    })
], searchResponsesnackbar);



/***/ }),

/***/ "./src/app/visiteur/signup/signup.component.css":
/*!******************************************************!*\
  !*** ./src/app/visiteur/signup/signup.component.css ***!
  \******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("ul>li {\r\n    padding-right: 10px;\r\n    color: #484848;\r\n}\r\ndiv,form,li,button {\r\n    font-family: myfont;\r\n    font-size: 14px ;\r\n    color: #484848;\r\n}\r\nbutton{\r\n    color: white;\r\n}\r\n.forgot {\r\n    font-size: 12px;\r\n    letter-spacing: 2px;\r\n    color: #484848;\r\n    margin-top:10px;\r\n    font-family: myfont;\r\n}\r\nul{\r\n    margin-top: 10px;\r\n}\r\ninput::-webkit-input-placeholder{\r\n    font-size: 12px;\r\n    font-family: myfont;\r\n}\r\ninput::-moz-placeholder{\r\n    font-size: 12px;\r\n    font-family: myfont;\r\n}\r\ninput::-ms-input-placeholder{\r\n    font-size: 12px;\r\n    font-family: myfont;\r\n}\r\ninput::placeholder{\r\n    font-size: 12px;\r\n    font-family: myfont;\r\n}\r\ninput{\r\n    font-family: 'Heebo', sans-serif;\r\n    font-size: 14px;\r\n}\r\n.form-control:focus{\r\n    border-color: #006a70;\r\n    box-shadow: 0 0 0 0.2rem rgba(0, 106, 112, 0.25);\r\n    }\r\nbutton{\r\n    background-color: #006a70;\r\n}\r\nbutton:focus {\r\n    outline: none !important ;\r\n    box-shadow:none !important;\r\n  }\r\n::ng-deep .mat-progress-bar-fill::after {\r\n    background-color:#006a70;\r\n}\r\n::ng-deep .mat-progress-bar-buffer {\r\n    background: #50bac0;\r\n}\r\n::ng-deep .mat-progress-bar {\r\n    border-radius: 2px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlzaXRldXIvc2lnbnVwL3NpZ251cC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksbUJBQW1CO0lBQ25CLGNBQWM7QUFDbEI7QUFDQTtJQUNJLG1CQUFtQjtJQUNuQixnQkFBZ0I7SUFDaEIsY0FBYztBQUNsQjtBQUVBO0lBQ0ksWUFBWTtBQUNoQjtBQUNBO0lBQ0ksZUFBZTtJQUNmLG1CQUFtQjtJQUNuQixjQUFjO0lBQ2QsZUFBZTtJQUNmLG1CQUFtQjtBQUN2QjtBQUNBO0lBQ0ksZ0JBQWdCO0FBQ3BCO0FBQ0E7SUFDSSxlQUFlO0lBQ2YsbUJBQW1CO0FBQ3ZCO0FBSEE7SUFDSSxlQUFlO0lBQ2YsbUJBQW1CO0FBQ3ZCO0FBSEE7SUFDSSxlQUFlO0lBQ2YsbUJBQW1CO0FBQ3ZCO0FBSEE7SUFDSSxlQUFlO0lBQ2YsbUJBQW1CO0FBQ3ZCO0FBRUE7SUFDSSxnQ0FBZ0M7SUFDaEMsZUFBZTtBQUNuQjtBQUNBO0lBQ0kscUJBQXFCO0lBQ3JCLGdEQUFnRDtJQUNoRDtBQUNKO0lBQ0kseUJBQXlCO0FBQzdCO0FBQ0E7SUFDSSx5QkFBeUI7SUFDekIsMEJBQTBCO0VBQzVCO0FBQ0E7SUFDRSx3QkFBd0I7QUFDNUI7QUFFQTtJQUNJLG1CQUFtQjtBQUN2QjtBQUVBO0lBQ0ksa0JBQWtCO0FBQ3RCIiwiZmlsZSI6InNyYy9hcHAvdmlzaXRldXIvc2lnbnVwL3NpZ251cC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsidWw+bGkge1xyXG4gICAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICAgIGNvbG9yOiAjNDg0ODQ4O1xyXG59XHJcbmRpdixmb3JtLGxpLGJ1dHRvbiB7XHJcbiAgICBmb250LWZhbWlseTogbXlmb250O1xyXG4gICAgZm9udC1zaXplOiAxNHB4IDtcclxuICAgIGNvbG9yOiAjNDg0ODQ4O1xyXG59XHJcblxyXG5idXR0b257XHJcbiAgICBjb2xvcjogd2hpdGU7XHJcbn1cclxuLmZvcmdvdCB7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBsZXR0ZXItc3BhY2luZzogMnB4O1xyXG4gICAgY29sb3I6ICM0ODQ4NDg7XHJcbiAgICBtYXJnaW4tdG9wOjEwcHg7XHJcbiAgICBmb250LWZhbWlseTogbXlmb250O1xyXG59XHJcbnVse1xyXG4gICAgbWFyZ2luLXRvcDogMTBweDtcclxufVxyXG5pbnB1dDo6cGxhY2Vob2xkZXJ7XHJcbiAgICBmb250LXNpemU6IDEycHg7XHJcbiAgICBmb250LWZhbWlseTogbXlmb250O1xyXG59XHJcblxyXG5pbnB1dHtcclxuICAgIGZvbnQtZmFtaWx5OiAnSGVlYm8nLCBzYW5zLXNlcmlmO1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG59XHJcbi5mb3JtLWNvbnRyb2w6Zm9jdXN7XHJcbiAgICBib3JkZXItY29sb3I6ICMwMDZhNzA7XHJcbiAgICBib3gtc2hhZG93OiAwIDAgMCAwLjJyZW0gcmdiYSgwLCAxMDYsIDExMiwgMC4yNSk7XHJcbiAgICB9ICAgXHJcbmJ1dHRvbntcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMwMDZhNzA7XHJcbn1cclxuYnV0dG9uOmZvY3VzIHtcclxuICAgIG91dGxpbmU6IG5vbmUgIWltcG9ydGFudCA7XHJcbiAgICBib3gtc2hhZG93Om5vbmUgIWltcG9ydGFudDtcclxuICB9XHJcbiAgOjpuZy1kZWVwIC5tYXQtcHJvZ3Jlc3MtYmFyLWZpbGw6OmFmdGVyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IzAwNmE3MDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtcHJvZ3Jlc3MtYmFyLWJ1ZmZlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjNTBiYWMwO1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC1wcm9ncmVzcy1iYXIge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG59Il19 */");

/***/ }),

/***/ "./src/app/visiteur/signup/signup.component.ts":
/*!*****************************************************!*\
  !*** ./src/app/visiteur/signup/signup.component.ts ***!
  \*****************************************************/
/*! exports provided: SignupComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SignupComponent", function() { return SignupComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_modal_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../../services/modal.service */ "./src/app/services/modal.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_entity_user__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/entity/user */ "./src/app/entity/user.ts");
/* harmony import */ var _services_client_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../services/client/authentication.service */ "./src/app/services/client/authentication.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");








let SignupComponent = class SignupComponent {
    constructor(formBuilder, route, authenticationService, modal) {
        this.formBuilder = formBuilder;
        this.route = route;
        this.authenticationService = authenticationService;
        this.modal = modal;
        this.showLoader = false;
        this.isSubmited = false;
        this.msgError = "account already exist";
    }
    ngOnInit() {
        this.signupform = this.formBuilder.group({
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].email]),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(3)]),
            confirmpassword: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(3)]),
            firstname: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(3)]),
            lastname: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"]('', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].minLength(3)]),
        }, {
            validator: this.MustMatch('password', 'confirmpassword')
        });
    }
    onSubmit() {
        this.isSubmited = true;
        this.showLoader = true;
        this.isSubmited = true;
        this.showLoader = true;
        let user = new src_app_entity_user__WEBPACK_IMPORTED_MODULE_3__["User"]();
        user.email = this.signupform.value.email;
        user.firstName = this.signupform.value.firstname;
        user.lastName = this.signupform.value.lastname;
        this.authenticationService.signup(user, this.signupform.value.password)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["first"])())
            .subscribe(data => {
            if (data.status == 200) {
                alert("account created");
            }
        }, error => {
            console.error(error);
            this.isCreatedError = true;
            this.showLoader = false;
            this.isSubmited = false;
            //this.showLoader=false;
        }, () => {
            this.authenticationService.login(this.signupform.value.email, this.signupform.value.password)
                .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["first"])())
                .subscribe(data => {
                this.showLoader = false;
                if (data.status == 200) {
                    this.modal.hideLogin();
                    this.route.navigate(['/client']);
                }
            }, err => {
                console.error(err);
                this.isCreatedError = true;
                this.showLoader = false;
                this.isSubmited = false;
            });
        });
    }
    MustMatch(controlName, matchingControlName) {
        return (formGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];
            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                // return if another validator has already found an error on the matchingControl
                return;
            }
            // set error on matchingControl if validation fails
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({ mustMatch: true });
            }
            else {
                matchingControl.setErrors(null);
            }
        };
    }
};
SignupComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormBuilder"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _services_client_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"] },
    { type: _services_modal_service__WEBPACK_IMPORTED_MODULE_1__["ModalService"] }
];
SignupComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_6__["Component"])({
        selector: 'app-signup',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./signup.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/signup/signup.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./signup.component.css */ "./src/app/visiteur/signup/signup.component.css")).default]
    })
], SignupComponent);



/***/ }),

/***/ "./src/app/visiteur/visiteur-routing.module.ts":
/*!*****************************************************!*\
  !*** ./src/app/visiteur/visiteur-routing.module.ts ***!
  \*****************************************************/
/*! exports provided: VisiteurRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisiteurRoutingModule", function() { return VisiteurRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _search_response_search_response_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./search-response/search-response.component */ "./src/app/visiteur/search-response/search-response.component.ts");
/* harmony import */ var _search_request_search_request_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./search-request/search-request.component */ "./src/app/visiteur/search-request/search-request.component.ts");
/* harmony import */ var _visiteur_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./visiteur.component */ "./src/app/visiteur/visiteur.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");






const routes = [
    {
        path: '', component: _visiteur_component__WEBPACK_IMPORTED_MODULE_3__["VisiteurComponent"], children: [
            {
                path: 'req', component: _search_request_search_request_component__WEBPACK_IMPORTED_MODULE_2__["SearchRequestComponent"]
            },
            {
                path: 'rep/:categorie/:subject', component: _search_response_search_response_component__WEBPACK_IMPORTED_MODULE_1__["SearchResponseComponent"]
            },
            {
                path: '', redirectTo: 'req', pathMatch: 'full'
            },
        ]
    }
];
let VisiteurRoutingModule = class VisiteurRoutingModule {
};
VisiteurRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"]]
    })
], VisiteurRoutingModule);



/***/ }),

/***/ "./src/app/visiteur/visiteur.component.css":
/*!*************************************************!*\
  !*** ./src/app/visiteur/visiteur.component.css ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".main {\r\n    display: grid;\r\n}\r\n .router,.mynav {\r\n    \r\n    grid-area: 1/1;\r\n   \r\n  }\r\n ::ng-deep .mat-progress-bar-fill::after {\r\n    background-color:#006a70;\r\n}\r\n ::ng-deep .mat-progress-bar-buffer {\r\n    background: #50bac0;\r\n}\r\n ::ng-deep .mat-progress-bar {\r\n    border-radius: 2px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlzaXRldXIvdmlzaXRldXIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGFBQWE7QUFDakI7Q0FDQzs7SUFFRyxjQUFjOztFQUVoQjtDQUNBO0lBQ0Usd0JBQXdCO0FBQzVCO0NBRUE7SUFDSSxtQkFBbUI7QUFDdkI7Q0FFQTtJQUNJLGtCQUFrQjtBQUN0QiIsImZpbGUiOiJzcmMvYXBwL3Zpc2l0ZXVyL3Zpc2l0ZXVyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubWFpbiB7XHJcbiAgICBkaXNwbGF5OiBncmlkO1xyXG59XHJcbiAucm91dGVyLC5teW5hdiB7XHJcbiAgICBcclxuICAgIGdyaWQtYXJlYTogMS8xO1xyXG4gICBcclxuICB9XHJcbiAgOjpuZy1kZWVwIC5tYXQtcHJvZ3Jlc3MtYmFyLWZpbGw6OmFmdGVyIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IzAwNmE3MDtcclxufVxyXG5cclxuOjpuZy1kZWVwIC5tYXQtcHJvZ3Jlc3MtYmFyLWJ1ZmZlciB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjNTBiYWMwO1xyXG59XHJcblxyXG46Om5nLWRlZXAgLm1hdC1wcm9ncmVzcy1iYXIge1xyXG4gICAgYm9yZGVyLXJhZGl1czogMnB4O1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ "./src/app/visiteur/visiteur.component.ts":
/*!************************************************!*\
  !*** ./src/app/visiteur/visiteur.component.ts ***!
  \************************************************/
/*! exports provided: VisiteurComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisiteurComponent", function() { return VisiteurComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _services_loader_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../services/loader.service */ "./src/app/services/loader.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm2015/material.js");




let VisiteurComponent = class VisiteurComponent {
    constructor(_snackBar, loader) {
        this._snackBar = _snackBar;
        this.loader = loader;
        this.message = 'Snack Bar opened.';
        this.actionButtonLabel = 'Retry';
        this.action = true;
        this.setAutoHide = true;
        this.autoHide = 2000;
        this.horizontalPosition = 'center';
        this.verticalPosition = 'bottom';
    }
    navigateTo(element) {
        this.footer.nativeElement.scrollIntoView({ behavior: "smooth", block: "start" });
    }
    openSnackBar() {
        let config = new _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBarConfig"]();
        config.verticalPosition = this.verticalPosition;
        config.horizontalPosition = this.horizontalPosition;
        config.duration = this.setAutoHide ? this.autoHide : 0;
        this._snackBar.open(this.message, this.action ? this.actionButtonLabel : undefined, config);
    }
    ngOnInit() {
        this.progressloader();
    }
    progressloader() {
        this.loader.currentLoader.subscribe(data => {
            if (data) {
                this.showLoader = true;
            }
            else {
                this.showLoader = false;
            }
        });
    }
};
VisiteurComponent.ctorParameters = () => [
    { type: _angular_material__WEBPACK_IMPORTED_MODULE_3__["MatSnackBar"] },
    { type: _services_loader_service__WEBPACK_IMPORTED_MODULE_1__["LoaderService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["ViewChild"])("footer", { static: false })
], VisiteurComponent.prototype, "footer", void 0);
VisiteurComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["Component"])({
        selector: 'app-visiteur',
        template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! raw-loader!./visiteur.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/visiteur/visiteur.component.html")).default,
        styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(/*! ./visiteur.component.css */ "./src/app/visiteur/visiteur.component.css")).default]
    })
], VisiteurComponent);



/***/ }),

/***/ "./src/app/visiteur/visiteur.module.ts":
/*!*********************************************!*\
  !*** ./src/app/visiteur/visiteur.module.ts ***!
  \*********************************************/
/*! exports provided: VisiteurModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisiteurModule", function() { return VisiteurModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _visiteur_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./visiteur.component */ "./src/app/visiteur/visiteur.component.ts");
/* harmony import */ var _search_request_search_request_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./search-request/search-request.component */ "./src/app/visiteur/search-request/search-request.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _visiteur_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./visiteur-routing.module */ "./src/app/visiteur/visiteur-routing.module.ts");
/* harmony import */ var _search_response_search_response_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./search-response/search-response.component */ "./src/app/visiteur/search-response/search-response.component.ts");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm2015/flex-layout.js");
/* harmony import */ var _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @fortawesome/angular-fontawesome */ "./node_modules/@fortawesome/angular-fontawesome/fesm2015/angular-fontawesome.js");
/* harmony import */ var _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./navbar/navbar.component */ "./src/app/visiteur/navbar/navbar.component.ts");
/* harmony import */ var _shared_shared_module__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../shared/shared.module */ "./src/app/shared/shared.module.ts");
/* harmony import */ var _app_material_app_material_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../app-material/app-material.module */ "./src/app/app-material/app-material.module.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./login/login.component */ "./src/app/visiteur/login/login.component.ts");
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./signup/signup.component */ "./src/app/visiteur/signup/signup.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");















let VisiteurModule = class VisiteurModule {
};
VisiteurModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["NgModule"])({
        declarations: [
            _visiteur_component__WEBPACK_IMPORTED_MODULE_1__["VisiteurComponent"],
            _search_request_search_request_component__WEBPACK_IMPORTED_MODULE_2__["SearchRequestComponent"],
            _search_response_search_response_component__WEBPACK_IMPORTED_MODULE_6__["SearchResponseComponent"],
            _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_9__["NavbarComponent"],
            _login_login_component__WEBPACK_IMPORTED_MODULE_12__["LoginComponent"],
            _signup_signup_component__WEBPACK_IMPORTED_MODULE_13__["SignupComponent"],
            _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_9__["HelpSnackBar"],
            _search_request_search_request_component__WEBPACK_IMPORTED_MODULE_2__["TermsComponent"],
            _search_response_search_response_component__WEBPACK_IMPORTED_MODULE_6__["searchResponsesnackbar"]
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
            _visiteur_routing_module__WEBPACK_IMPORTED_MODULE_5__["VisiteurRoutingModule"],
            _angular_flex_layout__WEBPACK_IMPORTED_MODULE_7__["FlexLayoutModule"],
            _fortawesome_angular_fontawesome__WEBPACK_IMPORTED_MODULE_8__["FontAwesomeModule"],
            _shared_shared_module__WEBPACK_IMPORTED_MODULE_10__["SharedModule"],
            _app_material_app_material_module__WEBPACK_IMPORTED_MODULE_11__["AppMaterialModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_14__["ReactiveFormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_14__["FormsModule"]
        ],
        entryComponents: [
            _login_login_component__WEBPACK_IMPORTED_MODULE_12__["LoginComponent"],
            _signup_signup_component__WEBPACK_IMPORTED_MODULE_13__["SignupComponent"],
            _navbar_navbar_component__WEBPACK_IMPORTED_MODULE_9__["HelpSnackBar"],
            _search_request_search_request_component__WEBPACK_IMPORTED_MODULE_2__["TermsComponent"],
            _search_response_search_response_component__WEBPACK_IMPORTED_MODULE_6__["searchResponsesnackbar"]
        ]
    })
], VisiteurModule);



/***/ })

}]);
//# sourceMappingURL=visiteur-visiteur-module-es2015.js.map